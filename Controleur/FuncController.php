<?php
/**
* Fichier du Controlleur
*/
if (file_exists("index.php")){
    $linkIndex = './';
}
else {
    $linkIndex = '../';
}

include_once 'Controller.php';
include_once $linkIndex.'Vue/menuBarre.php';
include_once $linkIndex.'Modele/utilisateur.php';
include_once $linkIndex.'Modele/fruit.php';
include_once $linkIndex.'Modele/image.php';
include_once $linkIndex.'Modele/commande.php';
include_once $linkIndex.'Modele/acces.php';
include_once $linkIndex.'Modele/categorieCritere.php';
include_once $linkIndex.'Modele/critere.php';

/**
*
* Classe permettant de faire le lien entre la Vue et le Modele.
* Elle rassemble les fonctions de transition, de vérification et de test du site.
*/
class FuncController extends Controller{
    public  function __construct(){
        $this->tab=array(
			"creerCompteClient" => "creerCompteClient",
			"creerCompteClientAdmin" => "creerCompteClientAdmin",
			"creerDiner" => "creerDiner",
			"creerFruitAdmin" => "creerFruitAdmin",
			"creerCategorieCritere" => "creerCategorieCritere",
			"creerCritere" => "creerCritere",
			"participer" => "participer",
			"noterDiner" => "noterDiner",
			"getAllFruits" => "getAllFruits",
			"getAllUsers" => "getAllUsers",
			"getInfoClientByIdu" => "getInfoClientByIdu",
			"getInfoFruitByIdf" => "getInfoFruitByIdf",
			"getAllCommandesByIdu" => "getAllCommandesByIdu",
			"getAllDinerAvenirByIdu" => "getAllDinerAvenirByIdu",
			"rechercherDiner" => "rechercherDiner",
			"getNbParticipantsByIdd" => "getNbParticipantsByIdd",
			"getResaEnCoursByIdu" => "getResaEnCoursByIdu",
			"getAllAcces" => "getAllAcces",
			"getAllCategorieCritere" => "getAllCategorieCritere",
			"getAllCritere" => "getAllCritere",
			"getAllCritereByIdcc" => "getAllCritereByIdcc",
			"getNoteMoyenneHoteByIdu" => "getNoteMoyenneHoteByIdu",
			"getNoteMoyenneHoteByIdd" => "getNoteMoyenneHoteByIdd",
			"getNoteMoyenneInviteByIdu" => "getNoteMoyenneInviteByIdu",
			"getNoteInviteByIdd" => "getNoteInviteByIdd",
			"dinerDejaNote" => "dinerDejaNote",
			"modifCompteAbonne" => "modifCompteAbonne",
			"modifCompteAdmin" => "modifCompteAdmin",
			"modifSolde" => "modifSolde",
			"modifierCommande" => "modifierCommande",
			"modifFruitAdmin" => "modifFruitAdmin",
			"annulerDiner" => "annulerDiner",
			"annulerResa" => "annulerResa",
			"contactAdmin" => "contactAdmin",
			"get3LatestDiners" => "get3LatestDiners",
			"supprimerUtilisateurAdm" => "supprimerUtilisateurAdm",
            "insert_resa" => "insert_resa",
            "justDoIt" => "justDoIt",
            "retirerSolde" => "retirerSolde",
            "getSolde" => "getSolde",
            "getResaEnCours" => "getResaEnCours",
            "getCapacite" => "getCapacite",
			"upload" => "upload"
        );
    }
	
	//Fonction utilisée lors de la création d'un compte client depuis la page principale
    public function creerCompteClient(){
        // Chargement de la barre de navigation
        session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }

        // Début des vérifications de tous les paramètres.
        $u = new utilisateur();
        $utilisateur = $u->getAllEmail();
        $bool=true;
        $res='';
        if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }

        if (empty($_POST['prenom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le prenom doit être renseigné.</div>';
            $bool=false;
        } else {
            $prenom = strip_tags(htmlentities($_POST['prenom']));
        }

        if (empty($_POST['addresse'])) {
            $res.='<div class="alert alert-danger" role="alert">L\'addresse doit être renseigné.</div>';
            $bool=false;
        } else {
            $addr = strip_tags(htmlentities($_POST['addresse']));
        }

        if (empty($_POST['codePostal'])) {
            $res.='<div class="alert alert-danger" role="alert">Le codePostal doit être renseigné.</div>';
            $bool=false;
        } else {
            $cp = strip_tags(htmlentities($_POST['codePostal']));
        }

        if (empty($_POST['ville'])) {
            $res.='<div class="alert alert-danger" role="alert">La ville doit être renseignée.</div>';
            $bool=false;
        } else {
            $ville = strip_tags(htmlentities($_POST['ville']));
        }

        if (empty($_POST['tel'])) {
            $res.='<div class="alert alert-danger" role="alert">Le téléphone doit être renseigné.</div>';
            $bool=false;
        } else {
            $tel = strip_tags(htmlentities($_POST['tel']));
        }

        if (empty($_POST['mail'])) {
            $res.='<div class="alert alert-danger" role="alert">Le mail doit être renseigné.</div>';
            $bool=false;
        } else {
            $mail = strip_tags(htmlentities($_POST['mail']));
        }
        if (empty($_POST['mdp'])) {
            $res.='<div class="alert alert-danger" role="alert">Le mot de passe doit être renseigné.</div>';
            $bool=false;
        } else {
            $mdp = strip_tags(htmlentities($_POST['mdp']));
        }

        // On vérifie que l'Email n'est pas déjà utilisé par un autre compte
        foreach($utilisateur as $t){
            if($t['email']==$_POST['mail']){
                $res.='<div class="alert alert-danger" role="alert">L\'adresse saisis exite déjà.</div>';
                $bool=false;
                break;
            }
        }

        // Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Création effectuée avec succès !</div>';
            $u->insertClient($nom,$prenom, $addr, $cp, $ville, $tel, $mail,$mdp);
        }

        // On affiche la page de retour
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
    <link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
    '.$res.'
    </div>';
    }

	//Fonction utilisée lors de la création d'un compte client par un administrateur
    public function creerCompteClientAdmin(){
        // Chargement de la barre de navigation
        session_start();
        $barre = "barreVisiteur";

        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }

        // Début des vérifications de tous les paramètres.
        $u = new utilisateur();
        $utilisateur = $u->getAllEmail();
        $bool=true;
        $res='';
        if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }

        if (empty($_POST['prenom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le prenom doit être renseigné.</div>';
            $bool=false;
        } else {
            $prenom = strip_tags(htmlentities($_POST['prenom']));
        }

        if (empty($_POST['addresse'])) {
            $res.='<div class="alert alert-danger" role="alert">L\'addresse doit être renseigné.</div>';
            $bool=false;
        } else {
            $addr = strip_tags(htmlentities($_POST['addresse']));
        }

        if (empty($_POST['codePostal'])) {
            $res.='<div class="alert alert-danger" role="alert">Le codePostal doit être renseigné.</div>';
            $bool=false;
        } else {
            $cp = strip_tags(htmlentities($_POST['codePostal']));
        }

        if (empty($_POST['ville'])) {
            $res.='<div class="alert alert-danger" role="alert">La ville doit être renseignée.</div>';
            $bool=false;
        } else {
            $ville = strip_tags(htmlentities($_POST['ville']));
        }

        if (empty($_POST['tel'])) {
            $res.='<div class="alert alert-danger" role="alert">Le téléphone doit être renseigné.</div>';
            $bool=false;
        } else {
            $tel = strip_tags(htmlentities($_POST['tel']));
        }

        if (empty($_POST['mail'])) {
            $res.='<div class="alert alert-danger" role="alert">Le mail doit être renseigné.</div>';
            $bool=false;
        } else {
            $mail = strip_tags(htmlentities($_POST['mail']));
        }

        if (empty($_POST['mdp'])) {
            $res.='<div class="alert alert-danger" role="alert">Le mot de passe doit être renseigné.</div>';
            $bool=false;
        } else {
            $mdp = strip_tags(htmlentities($_POST['mdp']));
        }

        if (empty($_POST['droit'])){
            $res .='<div class=alert alert-danger" role="alert">Les droits doivent être renseignés.</div>';
            $bool=false;
        } else {
            $droit = strip_tags(htmlentities($_POST['droit']));
        }

        // On vérifie que l'Email n'est pas déjà utilisé par un autre compte
        foreach($utilisateur as $t){
            if($t['email']==$_POST['mail']){
                $res.='<div class="alert alert-danger" role="alert">L\'adresse saisis exite déjà.</div>';
                $bool=false;
                break;
            }
        }

        // Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Création effectuée avec succès !</div>';
            $u->insertClientAdm($droit, $nom,$prenom, $addr, $cp, $ville, $tel, $mail,$mdp);
        }

        // On affiche la page de retour
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
    <link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
    '.$res.'
    </div>';
    }

	//Fonction utilisée lors de la création d'un diner par le menu de navigation
    //L'organisateur de ce diner sera le compte créateur
    public function creerDiner(){
	/*	
		// Chargement de la barre de navigation
        session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }

        // Début des vérifications de tous les paramètres.
        $bool=true;
        $res='';
        $message='';
        if (empty($_POST['date'])){
            $res.='<div class="alert alert-danger" role="alert">La date doit être choisie.</div>';
            $bool=false;
        } 
        elseif((time()-(60*60*24)) > strtotime($_POST['date'])){ //Si la date est avant aujourd'hui
            $res.='<div class="alert alert-danger" role="alert">La date ne peut pas être antérieure à ce jour.</div>';
            $bool=false;            
        }
        else {
            $date = strip_tags(htmlentities($_POST['date']));
        }

        if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }

        if (empty($_POST['lieu'])) {
            $res.='<div class="alert alert-danger" role="alert">Le lieu doit être renseigné.</div>';
            $bool=false;
        } else {
            $lieu = strip_tags(htmlentities($_POST['lieu']));
        }

        if (empty($_POST['desc'])) {
            $res.='<div class="alert alert-danger" role="alert">La description doit être complétée.</div>';
            $bool=false;
        } else {
            $desc = strip_tags(htmlentities($_POST['desc']));
        }

        if (empty($_POST['prix'])) {
            $res.='<div class="alert alert-danger" role="alert">Le prix doit être renseigné.</div>';
            $bool=false;
        } else {
            $prix = strip_tags(htmlentities($_POST['prix']));
        }

        if (empty($_POST['capa'])) {
            $res.='<div class="alert alert-danger" role="alert">La capacité doit être renseignée.</div>';
            $bool=false;
        } else {
            $capa = strip_tags(htmlentities($_POST['capa']));
        }

        if (empty($_POST['critere'])) {
            $res.='<div class="alert alert-danger" role="alert">Un critère doit être sélectionné.</div>';
            $bool=false;
        } else {
            $critere = strip_tags(htmlentities($_POST['critere']));
        }

        if (empty($_POST['image'])) {
            $res.='<div class="alert alert-danger" role="alert">Une image doit être insérée.</div>';
            $bool=false;
        } else {
            $image = strip_tags(htmlentities($_POST['image']));

            //>>>>>>>>>>>>>>>>>>>>>>>> SCRIPT VERIFICATION IMAGE <<<<<<<<<<<<<<<<<<<<<<<<<<<<

            define('TARGET', $_SERVER['DOCUMENT_ROOT'] . '/ter/Images/'); // Repertoire cible
            define('MAX_SIZE', 500000); // Taille max en octets du fichier
            define('WIDTH_MAX', 5000); // Largeur max de l'image en pixels
            define('HEIGHT_MAX', 5000); // Hauteur max de l'image en pixels
            // Tableaux de donnees
            $tabExt = array('jpg', 'gif', 'png', 'jpeg'); // Extensions autorisees
            $infosImg = array();
            // Variables
            $extension = '';
            $nomImage = '';

			// Création du répertoire d'upload
            if (!is_dir(TARGET)) {
                if (!mkdir(TARGET, 0755)) {
                    exit('Erreur : le répertoire cible ne peut-être créé ! Vérifiez que vous diposiez des droits suffisants pour le faire ou créez le manuellement !');
                }
            }
             // Script d'upload

             if (!empty($_POST['image'])&&$bool) {
// On verifie si le champ est rempli
                if (!empty($_FILES['fichier']['name'])) {
// Recuperation de l'extension du fichier
                    $extension = pathinfo($_FILES['fichier']['name'], PATHINFO_EXTENSION);
// On verifie l'extension du fichier
                    if (in_array(strtolower($extension), $tabExt)) {
// On recupere les dimensions du fichier
                        $infosImg = getimagesize($_FILES['fichier']['tmp_name']);
// On verifie le type de l'image
                        if ($infosImg[2] >= 1 && $infosImg[2] <= 14) {
// On verifie les dimensions et taille de l'image
                            if (($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($_FILES['fichier']['tmp_name']) <= MAX_SIZE)) {
// Parcours du tableau d'erreurs
                                if (isset($_FILES['fichier']['error'])
                                    && UPLOAD_ERR_OK === $_FILES['fichier']['error']
                                ) {
// On nomme le fichier
                                    $nomImage = basename(strip_tags(htmlentities($_POST['image'])));
//On verifie qu'aucun fichier du même nom existe
                                    if (!file_exists(TARGET . $nomImage)) {
// Si c'est OK, on teste l'upload
                                        if (move_uploaded_file($_FILES['fichier']['tmp_name'], TARGET . $nomImage)) {
                                            move_uploaded_file($_FILES['fichier']['tmp_name'], TARGET . $nomImage);
                                            $image = 'Images/' . $nomImage;
                                            $message = 'L\'opération a été effectuée avec succès!';
                                        } else {
// Sinon on affiche une erreur systeme
                                            $message = 'Problème lors de l\'enregistrement de l\'image !';
                                            $message .= '<br/>L\'enregistrement de l\'image n\'a donc pas été pris en compte.';
                                            $bool=false;
                                        }
                                    } else {
                                        $image = 'Images/' . $nomImage;
                                        $message = 'L\'image <strong>'.$nomImage.'</strong> existe déjà. Veuillez modifier le nom du fichier.';
                                        $message .= '<br/>L\'enregistrement de l\'image n\'a donc pas été pris en compte.';
                                        $bool=false;
                                    }
                                } else {
                                    $message = 'Une erreur interne a empêché l\'enregistrement de l\'image';
                                    $message .= '<br/>L\'enregistrement de l\'image n\'a donc pas été pris en compte.';
                                    $bool=false;
                                }
                            } else {
// Sinon erreur sur les dimensions et taille de l'image
                                $message = 'Erreur dans les dimensions de l\'image !';
                                $message .= '<br/>L\'enregistrement de l\'image n\'a donc pas été pris en compte.';
                                $bool=false;
                            }
                        } else {
// Sinon erreur sur le type de l'image
                            $message = 'Le fichier à uploader n\'est pas une image !';
                            $message .= '<br/>L\'enregistrement de l\'image n\'a donc pas été pris en compte.';
                            $bool=false;
                        }
                    } else {
// Sinon on affiche une erreur pour l'extension
                        $message = 'L\'extension du fichier est incorrecte ! Extension attendue : .jpg, .gif, .png, .jpeg';
                        $bool=false;
                    }
                } else {
// Sinon on affiche une erreur pour le champ vide
                    $message = 'Veillez insérer une image!';
                    $bool=false;
                }
            }
        }
        //>>>>>>>>>>>>>>>>>>>>>>>> SCRIPT VERIFICATION IMAGE <<<<<<<<<<<<<<<<<<<<<<<<<<<<

        // Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Création effectuée avec succès !</div>';
            $d = new diner();
            $d->insert($id, $nom, $lieu, $desc, $prix, $date, $capa,$image,$critere);
        }


        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-warning" role="alert">
       '.$res.$message.'
    </div>';*/

    }

    //Fonction utilisée lors de la création d'un diner par un administrateur
    //L'organisateur de ce diner est choisit parmis les comptes existants
    public function creerFruitAdmin(){
		 // Chargement de la barre de navigation
        session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }

        // Début des vérifications de tous les paramètres.
        $bool=true;
        $res='';
        if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }

        if (empty($_POST['desc'])) {
            $res.='<div class="alert alert-danger" role="alert">La description doit être complétée.</div>';
            $bool=false;
        } else {
            $desc = strip_tags(htmlentities($_POST['desc']));
        }

        if (empty($_POST['prix'])) {
            $res.='<div class="alert alert-danger" role="alert">Le prix doit être renseigné.</div>';
            $bool=false;
        } else {
            $prix = strip_tags(htmlentities($_POST['prix']));
        }

		// On déplace l'image
		if(!$this->upload('image')){
			$res.='<div class="alert alert-danger" role="alert">L\'image n\'a pas pu être chargée.</div>';
			$bool=false;
		} else {
			$image = "Images/". $_FILES['image']['name'];
		}		

		//Check des criteres
		$tabCrit = array();
		$ccriteres = $this->getAllCategorieCritere();
		foreach($ccriteres as $ccritere){
			if(isset($_POST[$ccritere->idcc])){
				$tabCrit[] = $_POST[$ccritere->idcc];
			}
		}
	
        // Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Création effectuée avec succès !</div>';
            $d = new fruit();
            $d->insert($nom, $desc, $prix, $tabCrit, $image);
        }

        //Affichage
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un fruit?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
        L\'insertion de votre fruit a été effectué avec
        <a href="#" class="alert-link">Succès !</a>
    </div>';
    }
	
	// Fonction utilisée lors de la création d'une nouvelle catégorie critère	
	public function creerCategorieCritere(){
		// Chargement de la barre de navigation
		session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }
		
		// Début de vérifications de tous les paramètres.
		$bool=true;
		$res='';
		if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }
		
		// Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Création effectuée avec succès !</div>';
            $c = new categorieCritere();
            $c->insert($nom);
        }
		
		//Affichage
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
        L\'insertion de votre dîner a été effectué avec
        <a href="#" class="alert-link">Succès !</a>
    </div>';
	}
	
	// Fonction utilisée lors de la création d'un nouveau critère
	public function creerCritere(){
		// Chargement de la barre de navigation
		session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }
		
		// Début des vérifications de tous les paramètres.
        $bool=true;
        $res='';
        if (empty($_POST['idcc'])){
            $res .='<div class=alert alert-danger" role="alert">La catégorie du critère doit etre renseignés.</div>';
            $bool=false;
        } else {
            $idcc = strip_tags(htmlentities($_POST['idcc']));
        }
		if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }
		
		// Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Création effectuée avec succès !</div>';
            $c = new critere();
            $c->insert($nom, $idcc);
        }

        //Affichage
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
        L\'insertion de votre dîner a été effectué avec
        <a href="#" class="alert-link">Succès !</a>
    </div>';
    }
	
	// Fonction permettant de s'inscrire à un diner
    public function participer(){
/*
        $f = new FuncController();
        $bool = $f->justDoIt($_POST['idu'],$_POST['idd'],$_POST['date'],$_POST['prix']);
        $utilisateur = $f->getSolde($_POST['idu']);
        session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>';
        if($bool) {
            echo '<div class="alert alert-success" role="alert">
                    <a class="alert-link">Félicitation!</a> Vous participez à ce diner.
                    Votre nouveau solde est de <a class="alert-link">'.$utilisateur[0]['solde'].'</a>
                  </div>';
        }
        else{
            echo '<div class="alert alert-warning" role="alert">
                    <a href="#" class="alert-link">Attention!</a> Votre solde est insuffisant. Pensez à le recharger.
                    <table id="table" class="table-condensed">
                        <tr>
                            <th>Votre Solde</th>
                            <th>Prix</th>
                        </tr>
                        <tr>
                            <td>'.$utilisateur[0]['solde'].'€</td>
                            <td>'.$_POST['prix'].'€</td>
                        </tr>
                    </table>
                  </div>';
        }*/
    }
	
	// Fonction permettant à un utilisateur de donner une note à un diner
	public function noterDiner(){
		/* // Chargement de la barre de navigation
		session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }
		
		// Début des vérifications de tous les paramètres.
        $bool=true;
        $res='';
		$idd = $_POST['diner'];
		if (empty($_POST['note'])) {
            $res.='<div class="alert alert-danger" role="alert">Une note doit être donnée.</div>';
            $bool=false;
        } else {
            $note = strip_tags(htmlentities($_POST['note']));
        }
		
		$d = $this->getInfoDinerByIdd($idd);
		$idu_Hot = $d->idu;
		
		// Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Notation effectuée avec succès !</div>';
            $nH = new noteHote();
            $nH->insert($idd, $idu_Hot, $id, $note);
        }

        //Affichage
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
        L\'insertion de votre dîner a été effectué avec
        <a href="#" class="alert-link">Succès !</a>
    </div>';*/
	}
	
	//Fonction permettant de récupérer les infos d'un fruit selon son idf
	public function getInfosFruit($idf){
		/*$f = new fruit();
		return $f->getInfosByIdf($idf);*/
	}
	
	//Fonction permettant de récupérer tous les fruit
	public function getAllFruits(){
		$f = new fruit();
		return $f->getAllFruitsInfos();
	}
	
    //Fonction utilisée pour obtenir l'ensemble des comptes existants
    public function getAllUsers(){
        $u = new utilisateur();
        return $u->getAll();
    }	
	
	// Fonction permettant de récupérer les infos d'un compte donné
	public function getInfoClientByIdu($id){
		$u = new utilisateur();
		$info = $u->getId($id);
		return $info;
	}

	// Fonction permettant de récupérer les infos d'un diner donné
	public function getInfoFruitByIdf($idf) {
		$d = new fruit();
		return $d->getInfosFruit($idf);
	}

	// Fonction permettant de récupérer les infos de tous les diners à venir d'un hote donné
	public function getAllCommandesByIdu($idu) {
		$d = new commande();
		return $d->getAllCommandes($idu);
	}
	
	// Fonction utilisée pour la recherche de diner
	// Les vérifications de formulaires se font dans le fichier Modele/diner.php
    public function rechercherDiner($idu,$nom,$date,$prix,$capa,$critere,$lieu){
        /*$d = new diner();
        return $d->rechercher($idu,$nom,$date,$prix,$capa,$critere,$lieu);*/
    }

	// Fonction permettant d'obtenir le nombre de participants à un diner donné
	public function getNbParticipantsByIdd($id){
		/*$r = new reservation();
		return $r->getNbParticipants($id);*/
	}
	
	// Fonction permettant de récupérer les informations des réservations en cours d'un compte donné
	public function getResaEnCoursByIdu($id){
		/*$r = new reservation();
		return $r->getResaEnCours($id);*/
	}
	
	// Fonction permettant de récupérer la liste des niveaux d'accès
	public function getAllAcces(){
		$a = new acces();
		return $a->getAll();
	}
	
	// Fonction permettant de récupérer la liste des catégorie de critère
	public function getAllCategorieCritere(){
		$cc = new categorieCritere();
		return $cc->getAll();
	}
	
	// Fonction permettant de récupérer la liste des critères
    public function  getAllCritere(){
	    /*$c = new critere();
	    return $c->getAll();*/
    }
	
	// Fonction permettant de récupérer la liste des critères ayant une catégorie donnée
	public function getAllCritereByIdcc($idcc){
		$c = new critere();
		return $c->getAllByIdcc($idcc);
	}

	// Fonction permettant de récupérer la note moyenne d'hote d'un compte donné
	public function getNoteMoyenneHoteByIdu($id){
		/*$nh = new noteHote();
		return $nh->getMoyenneHote($id);*/
	}
	
	// Fonction permettant de récupérer la note moyenne d'hote pour un diner donné
	public function getNoteMoyenneHoteByIdd($id){
		/*$nh = new noteHote();
		return $nh->getMoyenneDiner($id);*/
	}
	
	// Fonction permettant de récupérer la note moyenne d'invité d'un compte donné
	public function getNoteMoyenneInviteByIdu($id){
		/*$ni = new noteInvite();
		return $ni->getMoyenneInvite($id);*/
	}
	
	// Fonction permettant de récupérer la note d'invité d'un compte pour un diner donné
	public function getNoteInviteByIdd($idu, $idd){
		/*$ni = new noteInvite();
		return $ni->getNoteInvite($idu, $idd);*/
	}
	
	// Fonction permettant de savoir si un diner a déjà été noté par un utilisateur
	public function dinerDejaNote($idd, $idu){
		/*$nH = new noteHote();
		return $nH->getAlreadyNoted($idd, $idu);*/		
	}
 
	//Fonction qui permet à un abonné de modifier son compte
    public function modifCompteAbonne(){   
    /* // Chargement de la barre de navigation
    session_start();
    $barre = "barreVisiteur";
    if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
    {
        $grade=$_SESSION['acces'];
        $id=$_SESSION['idu'];

        switch($grade) {
            case "Abonne":
                $barre = "barreAbonne";
                break;
            case "Administrateur":
                $barre = "barreAdmin";
                break;
        }
    }else{
        if(isset($grade))
            unset($grade);
    }

        // Début des vérifications de tous les paramètres. 
		$u = new utilisateur();
		$bool=true;
        $res='';
		$idu = $_POST['idu'];
		
        if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }

        if (empty($_POST['prenom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le prenom doit être renseigné.</div>';
            $bool=false;
        } else {
            $prenom = strip_tags(htmlentities($_POST['prenom']));
        }

        if (empty($_POST['addresse'])) {
            $res.='<div class="alert alert-danger" role="alert">L\'addresse doit être renseigné.</div>';
            $bool=false;
        } else {
            $addr = strip_tags(htmlentities($_POST['addresse']));
        }

        if (empty($_POST['codePostal'])) {
            $res.='<div class="alert alert-danger" role="alert">Le codePostal doit être renseigné.</div>';
            $bool=false;
        } else {
            $cp = strip_tags(htmlentities($_POST['codePostal']));
        }

        if (empty($_POST['ville'])) {
            $res.='<div class="alert alert-danger" role="alert">La ville doit être renseignée.</div>';
            $bool=false;
        } else {
            $ville = strip_tags(htmlentities($_POST['ville']));
        }

        if (empty($_POST['tel'])) {
            $res.='<div class="alert alert-danger" role="alert">Le téléphone doit être renseigné.</div>';
            $bool=false;
        } else {
            $tel = strip_tags(htmlentities($_POST['tel']));
        }

        if (empty($_POST['mdpV'])) {
            $res.='<div class="alert alert-danger" role="alert">Pour valider les changement vous devez rentrer votre mot de passe actuel.</div>';
            $bool=false;
        } else {
            $mdpV = strip_tags(htmlentities($_POST['mdpV']));
            if(!$u->verifMdpClient($_SESSION['idu'], $mdpV)){
                $res.='<div class="alert alert-danger" role="alert">Mot de passe incorrect.</div>';
                $bool=false;
            }
        }

        // Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Modification de compte effectuée avec succès !</div>';

			$u->updateInfosClientNoMail($_SESSION['idu'], $nom, $prenom, $addr, $cp, $ville, $tel);
            if (empty($_POST['mdp1'])) {
                $res.='<div class="alert alert-danger" role="alert">Le mot de passe est inchangé.</div>';
            } else {
				if($_POST['mdp1'] == $_POST['mdp2']){
					$mdp = strip_tags(htmlentities($_POST['mdp1']));
					$u->updateMdpClient($_SESSION['idu'], $mdp);
					$res.= '<div class="alert alert-success" role="alert">Modification de mot de passe effectuée avec succès !</div>';
				}else{
					$res.='<div class="alert alert-danger" role="alert">Les deux mots de passes ne correspondent pas. Pas de changement de mot de passe.</div>';
				}
            }
        }

        // On affiche la page de retour
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
    <link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

$v = new menuBarre();
echo $v->affichage($barre);

echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
    '.$res.'
    </div>';*/
    }
    
	// Fonction permettant à un administrateur de modifier un compte
	public function modifCompteAdmin(){
	// Chargement de la barre de navigation
    session_start();
    $barre = "barreVisiteur";
    if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
    {
        $grade=$_SESSION['acces'];
        $id=$_SESSION['idu'];

        switch($grade) {
            case "Abonne":
                $barre = "barreAbonne";
                break;
            case "Administrateur":
                $barre = "barreAdmin";
                break;
        }
    }else{
        if(isset($grade))
            unset($grade);
    }

        // Début des vérifications de tous les paramètres. 
        $u = new utilisateur();
        $bool=true;
        $res='';
        if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }

        if (empty($_POST['prenom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le prenom doit être renseigné.</div>';
            $bool=false;
        } else {
            $prenom = strip_tags(htmlentities($_POST['prenom']));
        }

        if (empty($_POST['addresse'])) {
            $res.='<div class="alert alert-danger" role="alert">L\'addresse doit être renseigné.</div>';
            $bool=false;
        } else {
            $addr = strip_tags(htmlentities($_POST['addresse']));
        }

        if (empty($_POST['codePostal'])) {
            $res.='<div class="alert alert-danger" role="alert">Le codePostal doit être renseigné.</div>';
            $bool=false;
        } else {
            $cp = strip_tags(htmlentities($_POST['codePostal']));
        }

        if (empty($_POST['ville'])) {
            $res.='<div class="alert alert-danger" role="alert">La ville doit être renseignée.</div>';
            $bool=false;
        } else {
            $ville = strip_tags(htmlentities($_POST['ville']));
        }

        if (empty($_POST['tel'])) {
            $res.='<div class="alert alert-danger" role="alert">Le téléphone doit être renseigné.</div>';
            $bool=false;
        } else {
            $tel = strip_tags(htmlentities($_POST['tel']));
        }
		
		$idu = $_POST['idu'];
       	$acces = $_POST['acces'];
        

        // Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Modification de compte effectuée avec succès !</div>';
            $u->updateInfosClientNoMailAdmin($idu, $nom, $prenom, $addr, $cp, $ville, $tel, $acces);
            if (empty($_POST['mdp1'])) {
                $res.='<div class="alert alert-danger" role="alert">Le mot de passe est inchangé.</div>';
                } else {
                    if($_POST['mdp1'] == $_POST['mdp2']){
                        $mdp = strip_tags(htmlentities($_POST['mdp1']));
                        $u->updateMdpClient($idu, $mdp);
                        $res.= '<div class="alert alert-success" role="alert">Modification de mot de passe effectuée avec succès !</div>';
                    }
                    else{
                        $res.='<div class="alert alert-danger" role="alert">Les deux mots de passes ne correspondent pas. Pas de changement de mot de passe.</div>';
                    }

                }
        }

        // On affiche la page de retour
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
    <link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

$v = new menuBarre();
echo $v->affichage($barre);

echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
    '.$res.'
    </div>';
    }

	// Fonction permettant à un administrateur de modifier le solde d'un compte
	public function modifSolde(){
		// Chargement de la barre de navigation
		session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }
		
		// Début des vérifications de tous les paramètres.
        $bool=true;
        $res='';
		$idu = $_POST['idu'];
		
        if (empty($_POST['solde'])) {
            $res.='<div class="alert alert-danger" role="alert">Le solde doit être choisi.</div>';
            $bool=false;
        } else {
            $solde = strip_tags(htmlentities($_POST['solde']));
        }
		
		// Vérification de faisabilité et exécution
		$u = new utilisateur();
		$c = $u->getId($idu);
		
		foreach($c as $compte){
			$soldebd = $compte['solde'];
		}
		
		if($soldebd + $solde >= 0){
			$res = '<div class="alert alert-success" role="alert">Modifications effectuée avec succès !</div>';
			if($solde >= 0){
				$u->credSolde($idu, $solde);
			}else{
				$u->retirerSolde($idu, abs($solde));
			}
		}else{
			$res = '<span class=\"titre\">Erreur ...</span><br/>Le solde n\'a pas pu être modifié.<br/>';
		}

		// Affichage retour
		       echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="alert alert-warning" role="alert">
       '.$res.'
    </div>';
	}
	
	// Fonction permettant à un abonné de modifier un de ses diner
    public function modifierCommande($p){
		$msgErreur="";
        $d = new commande();
        $d = $d->getInfosCommande(strip_tags(htmlentities($p['idcom'])));

        if(empty($_POST['quantite'])){
            $msgErreur =  $msgErreur . "<span class=\"titre\">Erreur ...</span><br/>";
            $msgErreur =  $msgErreur . "La quantite doit être renseignée <br/>";
            $erreur=0;
        }else{
            $d->quantite=strip_tags(htmlentities($_POST['quantite']));
			//Modifier le prix total selon le prix unitaire
			$msgErreur = $msgErreur . "A ajouter dans FuncController.php, function modifierCommande l'update du prix<br/>";
			//$d->prixTotal=$d->quantite * $fruit->getPrixByIdf($idf);
        }

        $rep = $d->updateCommande($d);

        if ($rep > 0){
            $message = 'La commande n°'.$d->idcom.' a bien été modifié.';
        }else {
            $message = '<span class=\"titre\">Erreur ...</span><br/>'.'La commande n°'.$d->idcom.' n\'a pas pu être modifié.<br/>'. $msgErreur;
        }
        header('Location:./Vue/mesCommandes.php?message='.$message);
    }

	// Fonction permettant à un administrateur de modifier un diner
    public function modifFruitAdmin(){
		// Chargement de la barre de navigation
        session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }

        // Début des vérifications de tous les paramètres.
        $bool=true;
        $res='';
        if (empty($_POST['nom'])) {
            $res.='<div class="alert alert-danger" role="alert">Le nom doit être renseigné.</div>';
            $bool=false;
        } else {
            $nom = strip_tags(htmlentities($_POST['nom']));
        }

        if (empty($_POST['desc'])) {
            $res.='<div class="alert alert-danger" role="alert">La description doit être complétée.</div>';
            $bool=false;
        } else {
            $desc = strip_tags(htmlentities($_POST['desc']));
        }

        if (empty($_POST['prix'])) {
            $res.='<div class="alert alert-danger" role="alert">Le prix doit être renseigné.</div>';
            $bool=false;
        } else {
            $prix = strip_tags(htmlentities($_POST['prix']));
        }

		// On déplace l'image
		if($_FILES['image']['name'] != ''){
			if(!$this->upload('image')){
				$res.='<div class="alert alert-danger" role="alert">L\'image n\'a pas pu être chargée.</div>';
				$bool=false;
			} else {
				$image = "Images/". $_FILES['image']['name'];
			}
			$image == '';
		}

		//Check des criteres
		$tabCrit = array();
		$ccriteres = $this->getAllCategorieCritere();
		foreach($ccriteres as $ccritere){
			if(isset($_POST[$ccritere->idcc])){
				$tabCrit[] = $_POST[$ccritere->idcc];
			}
		}
	
        // Si l'on a aucune erreur, on lance la fonction
        if($bool){
            $res = '<div class="alert alert-success" role="alert">Création effectuée avec succès !</div>';
            $d = new fruit();
            $d->update($_POST['idf'], $nom, $desc, $prix, $tabCrit, $image);
        }

        //Affichage
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un fruit?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success" role="alert">
        L\'insertion de votre fruit a été effectué avec
        <a href="#" class="alert-link">Succès !</a>
    </div>';
    }

	// Fonction permettant de supprimer une commande
    public function annulerCommande($p){
        $idcom = strip_tags(htmlentities($p['idcom']));

        $d = new commande();
        $d = $d->getInfosCommande($idcom);

        $u = new utilisateur();
		
		// Suppression de la commande
		$rep = $d->deleteCommande($d->idcom);
		if($rep != 0){
			// Remboursement du paiement
			$rs = $u->credSolde($d->idu, $d->prixTotal);
			if($rs != 0){
				$msg = "La commande n°".$idcom." a bien été supprimé. Votre solde à été crédité de ".$d->prixTotal." €.";
			}else{
				$msg =  $msg . "<span class=\"titre\">Erreur ...</span><br/>";
                $msg =  $msg . "Le montant de " .$d->prixTotal. " n'a pas pu être ajouté à votre solde";
			}
		}
        
        header('Location:./Vue/mesDiners.php?message='.$msg);
    }

	// Fonction permettant d'annuler une réservation
    public function annulerResa($p){
       /* $id = 0;
		session_start();
        $id=$_SESSION['idu'];

        $r = new reservation();
        $r = $r->getInfosResa(strip_tags(htmlentities($p['idr'])));
        $montantRemb = $r->prix;
        $ra = new resaAnnulee();
        $ra->addResaAnnulee($r->idr, $id, $r->idd, $r->jour, $montantRemb);
        $r->deleteResa($r->idr);
        $u = new utilisateur();

        $u->credSolde($r->idu, $montantRemb);
        $message = 'La reservation n°'.$r->idr.' a bien été annulée.';

        header('Location:./Vue/mesResa.php?message='.$message);*/
    }

    //Fonction utilisé pour contacter l'administrateur
    //Ne produit rien si mauvais paramétrage du serveur smtp
    public function contactAdmin(){
        $target="victor.breton@u-psud.fr"; //Adresse de l'administrateur

        //Test sur le champ mail
        $passage_ligne = "\n";

        $message_txt = strip_tags(htmlentities($_POST['mail'])).$passage_ligne.$passage_ligne.strip_tags(htmlentities($_POST['msg']));

        $boundary = "-----=".md5(rand());

        $sujet = strip_tags(htmlentities($_POST['objet']));

        $head="FROM: \"Site de Diner\"<victor.breton@u-psud.fr>".$passage_ligne;
        $head.="Reply-to: \"Site de Diner\"<victor.breton@u-psud.fr>".$passage_ligne;
        $head.="MIME-Version: 1.0".$passage_ligne;
        $head.="Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;

        $message = $passage_ligne."--".$boundary.$passage_ligne;

        $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_txt.$passage_ligne;

        $message.= $passage_ligne."--".$boundary.$passage_ligne;

        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;

        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;

        mail($target,$sujet,$message,$head);

        header("Location:index.php");
    }

    //Fonction d'obtention des 3 derniers diners mis en ligne
    public function get3LatestDiners(){
		/*$d = new diner();
		return $d->get3Latest();*/
    }
	
	// Fonction permettant à un administrateur de supprimer un compte donné
	public function supprimerUtilisateurAdm(){
		// Chargement de la barre de navigation
		session_start();
        $barre = "barreVisiteur";
        if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
        {
            $grade=$_SESSION['acces'];
            $id=$_SESSION['idu'];

            switch($grade) {
                case "Abonne":
                    $barre = "barreAbonne";
                    break;
                case "Administrateur":
                    $barre = "barreAdmin";
                    break;
            }
        }else{
            if(isset($grade))
                unset($grade);
        }
		
		$res = '<div class="alert alert-success" role="alert">Suppression effectuée avec succès !</div>';
		$u = new utilisateur();
		$u->deleteUser($_POST['orga']);
		
		
        //Affichage
        echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
	<link type="text/Css" href="Css/menuBarre.Css" rel="stylesheet" />
    <link type="text/Css" href="Css/index.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/dist/Css/bootstrap.Css" rel="stylesheet" />
    <link type="text/Css" href="./bootstrap/datepicker/Css/datepicker.Css" rel="stylesheet"/>
    <link type="text/Css" href="./slider/Css/slider.Css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="./slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="./Js/rating.js"></script>

</head>
<body id="body">';

        $v = new menuBarre();
        echo $v->affichage($barre);

        echo '<div class="container">
    <div class="jumbotron">
        <h1 class="shadow" style="color: #ffffff">Besoin d\'un dîner?</h1>
        <p class="shadow" style="color: #ffffff">Ce site vous propose de rechercher des dîners près de chez vous rapidement !</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button" data-toggle="modal" data-target="#savoirPlus" style="cursor:pointer">En savoir plus</a></p>
        <!-- Modal -->
        <div class="modal fade" id="savoirPlus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Partage de diners en ligne</h4>
                    </div>
                    <div class="modal-body">
                        <p>Ce site web a été développé dans le cadre d\'un projet universitaire, au cours du M1 MIAGE à l\'Université Paris-Sud.</p>
                        <p>Il a pour but de faciliter le partage de diners entre particuliers en proposant deux fonctionnalités, très simples d\'utilisation.</p>
                        <p>Ainsi, vous pouvez proposer un dîner, organisé par vos soins, ou rechercher un dîner auquel participer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>'
	.$res;
	}

    public function insert_resa($idu,$idd,$date){
        /*$r = new reservation();
        if(!empty($idu) && !empty($idd) && !empty($date))
            $r->insert($idu,$idd,$date);*/
    }

    public function retirerSolde($idu,$solde){
        /*$u = new utilisateur();
        $u->retirerSolde($idu,$solde);*/
    }

    public function getSolde($id){
        /*$u = new utilisateur();
        return $u->getId($id);*/
    }

	//Participer à un diner avec verification du solde
	public function justDoIt($idu,$idd,$date,$prix){
        /*$f = new FuncController();
        $info=$f->getInfoClientByIdu($idu);
        if($info[0]['solde']-$prix >= 0) {
            $f->insert_resa($idu, $idd, $date);
            $f->retirerSolde($idu,$prix);
            return true;
        }
        return false;*/
    }

    public function getResaEnCours($id){
        /*$r = new reservation();
        return $r->getAll($id);*/
    }

    public function getCapacite($idd){
        /*$r = new reservation();
        return $r->getNbParticipants($idd);*/
    }

	public function upload($index,$maxsize=FALSE,$extensions=FALSE){
	   //Test1: fichier correctement uploadé
		 if (!isset($_FILES[$index]) OR $_FILES[$index]['error'] > 0) return FALSE;
	   //Test2: taille limite
		 if ($maxsize !== FALSE AND $_FILES[$index]['size'] > $maxsize) return FALSE;
	   //Test3: extension
		 $ext = substr(strrchr($_FILES[$index]['name'],'.'),1);
		 if ($extensions !== FALSE AND !in_array($ext,$extensions)) return FALSE;
	   //Déplacement
		if (file_exists("index.php")){
			$linkIndex = './';
		} else {
			$linkIndex = '../';
		}
		$destination = $linkIndex . "Images/" . $_FILES[$index]['name'];
		 return move_uploaded_file($_FILES[$index]['tmp_name'],$destination);
	}
}
?>
