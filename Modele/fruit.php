<?php
/**
* Fichier de Modele
*/

include_once 'image.php';
include_once 'commande.php';
include_once 'critere.php';


if (file_exists('base.php')){
    include_once 'base.php';
}
else {
    include_once '../base.php';
}
/**
* Classe permettant d'accéder à la table diner de la base de donnée
* La table diner définit les diners proposés sur le site.
*/
class fruit{
    /**
    * identifiant du diner
    * @access private
    *  @var integer
    */
    private $idf;

    /**
    * nom du diner
    * @access private
    *  @var string
    */
    private $nom;
    
    /**
     * date du diner
     * @access private
     *  @var 
     */
    private $photo;

    /**
    * description du diner
    * @access private
    *  @var string
    */
    private $desc;


    /**
    * prix du diner
    * @access private
    *  @var integer
    */
    private $prix;

    /**
     * Critère du diner
     * @access private
     *  @var tab[int]
     */
    private $criteres;

	public function __construct() {
    	
  	}

	// Fonction de getter
  	public function __get($attr_name) {
    	if (property_exists( __CLASS__, $attr_name)) {
      		return $this->$attr_name;
    	}
    	$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    	throw new Exception($emess, 45);
  	}

	// Fonction de setter
  	public function __set($attr_name, $attr_val) {
   		if (property_exists( __CLASS__, $attr_name)) {
      		$this->$attr_name = $attr_val;
    	}
    	$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    }


    // Fonction permettant d'ajouter un nouveau fruit dans la base
    public function insert($nom, $desc, $prix , $criteres, $image){
        $c = Base::getConnection();
        $query = $c->prepare("insert into fruit(nom,description,prix)
                          values(:nom,:description,:prix)");
        $query->bindParam (':nom',$nom, PDO::PARAM_STR);
        $query->bindParam (':description',$desc, PDO::PARAM_STR);
        $query->bindParam (':prix',$prix, PDO::PARAM_INT);
        $query->execute();

        $this->idf = $c->LastInsertId('fruit');
		
        //insertion de l'image
        $i = new image();
        $i->insert($this->idf,$image);
				
		//insertion des criteres
		foreach($criteres as $crit){
			$j = $c->prepare("insert into asso_fruit_critere(idf,idc)
							values(:idf,:idc)");
			$j->bindParam(":idf", $this->idf, PDO::PARAM_INT);
			$j->bindParam(":idc", $crit, PDO::PARAM_INT);
			$j->execute();			
		}
    }
	
	public function getAllFruitsInfos(){
		$c = Base::getConnection();
		$reponse = $c->query('SELECT * FROM fruit')->fetchAll();
		return $reponse;
	}

    //Fonction retournant les 3 derniers diners postés avec leurs images
    public function get3Latest(){
        /*$c = Base::getConnection();
        $query = $c->query("SELECT idd,idu,nom,date,lieu,description,prix,capacite FROM diner order by idd desc limit 3");
        $listeD = array();
        while ($k = $query->fetch()){
            $d = new diner();
            $i=new image();
            $d->idd = $k['idd'];
            $d->idu = $k['idu'];
            $d->nom = $k['nom'];
            $d->date = $k['date'];
            $d->lieu = $k['lieu'];
            $d->desc = $k['description'];
            $d->prix = $k['prix'];
            $d->capacite = $k['capacite']; 
            $d->photos= $i->getPhotos($k['idd']);
            $listeD[] = $d;
        }
        return $listeD;*/
    }

    // Fonction retournant la liste des diners à venir pour un hote donné
    public function getDinersAvenir($idu){
        /*$listeD = array();
        $r = new reservation();
        $c = Base::getConnection();
        $res = $c->query("select idd, idu, nom, date, lieu, description, prix, capacite from diner where date > CURDATE() AND idu = ".$idu);

        while ($donnees = $res->fetch()){
            $d = new diner();
            $d->idd = $donnees['idd'];
            $d->idu = $donnees['idu'];
            $d->nom = $donnees['nom'];
            $d->date = $donnees['date'];
            $d->lieu = $donnees['lieu'];
            $d->desc = $donnees['description'];
            $d->prix = $donnees['prix'];
            $d->capacite = $donnees['capacite']; 
            $d->nbPart = $r->getNbParticipants($d->idd);
            $listeD[] = $d;
        }
        return $listeD;*/
    }

    // Fonction retournant la liste des diners passés pour un hote donné (historique)
    public function getHistoDiners($idu){
        /*$listeD = array();
        $r = new reservation();
        $c = Base::getConnection();
        $res = $c->query("select idd, idu, nom, date, lieu, description, prix, capacite from diner where date <= CURDATE() AND idu = ".$idu);

        while ($donnees = $res->fetch()){
            $d = new diner();
            $d->idd = $donnees['idd'];
            $d->idu = $donnees['idu'];
            $d->nom = $donnees['nom'];
            $d->date = $donnees['date'];
            $d->lieu = $donnees['lieu'];
            $d->desc = $donnees['description'];
            $d->prix = $donnees['prix'];
            $d->capacite = $donnees['capacite']; 
            $d->nbPart = $r->getNbParticipants($d->idd);
            $listeD[] = $d;
        }
        return $listeD;*/
    }

	// Fonction retournant les informations d'un diner choisi par son idd
    public function getInfosFruit($idf){
        $c = Base::getConnection();
        $d = new fruit();
        $reponse = $c->query('SELECT * FROM fruit WHERE idf ='.$idf);
        $donnees = $reponse->fetch();
        $d->idf = $idf;
        $d->nom = $donnees['nom'];
        $d->desc = $donnees['description'];
        $d->prix = $donnees['prix'];
		
		$reponse = $c->query('SELECT idc FROM asso_fruit_critere WHERE idf='.$idf);
		$donnees = $reponse->fetchAll();
		foreach($donnees as $crit){
			$d->criteres[] = $crit['idc'];
		}
		
        return $d;
    }

	// Fonction retournant les diners répondants à la recherche
	public function rechercher($idu,$nom,$date ,$prix, $capa, $critere, $lieu){
        /*$c = base::getConnection();
        $request = "SELECT * FROM diner where 1";
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Debut Test <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        if (!empty($nom)){
            $nom = strip_tags(htmlentities($nom));
            $request.=" and nom LIKE '%".$nom."%'";
        }

        if (!empty($date)){
            $date = strip_tags(htmlentities($date));
            $request.=" and date='".$date."'";
        }
        if (!empty($prix)){
            $prix = strip_tags(htmlentities($prix));
            $request.=" and prix <= ".$prix." order by prix ASC";
        }

        if (!empty($capa)) {
            $capa = strip_tags(htmlentities($capa));
            $request.=" and capacite <= ".$capa;
        }

        if (!empty($critere)) {
            $critere = strip_tags(htmlentities($critere));
            $request.=" and idc=".$critere;
        }

        if (!empty($lieu)){
            $lieu = strip_tags(htmlentities($lieu));
            $request.=" and lieu LIKE '%".$lieu."%'";
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Fin Test <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        $query = $c->prepare($request); //where nom LIKE '%".$nom."'"
        $dbres = $query->execute();
        $d = $query->fetchAll();
        $tab = array();
        foreach ($d as $key => $donnees) {
            $d = new diner();
            $c = new critere();
            $i = new image();
            $d->idd = $donnees['idd'];
            $d->idu = $donnees['idu'];
            $d->nom = $donnees['nom'];
            $d->date = $donnees['date'];
            $d->desc = $donnees['description'];
            $d->lieu = $donnees['lieu'];
            $d->prix = $donnees['prix'];
            $d->capacite = $donnees['capacite'];
            $d->critere = $c->getId($donnees['idc']);
            $d->photos = $i->getAdd($donnees['idd']);
            $tab[] = $d;
        }
        return $tab;*/
    }
	
	// Fonction permettant de modifier un fruit
    public function update($idf, $nom, $desc, $prix, $tabCrit, $image){
        $c = Base::getConnection();
		
		//Suppression des anciennes informations
		$query = $c->prepare("DELETE FROM asso_fruit_critere WHERE idf = :idf");
		$query->bindParam(':idf', $idf, PDO::PARAM_INT);
		$query->execute();
		
		//Mise à jour des informations
		$query = $c->prepare("UPDATE fruit SET nom = :nom, description = :description, prix = :prix WHERE idf = :idf");
		$query->bindParam (':idf', $idf, PDO::PARAM_INT);
        $query->bindParam (':nom',$nom, PDO::PARAM_STR);
        $query->bindParam (':description',$desc, PDO::PARAM_STR);
        $query->bindParam (':prix',$prix, PDO::PARAM_INT);
        $query->execute();
		
		if($image != ''){
			$i = new image();
			$i->updateByIdf($idf, $image);
		}
	
		//insertion des criteres
		foreach($criteres as $crit){
			$j = $c->prepare("insert into asso_fruit_critere(idf,idc)
							values(:idf,:idc)");
			$j->bindParam(":idf", $this->idf, PDO::PARAM_INT);
			$j->bindParam(":idc", $crit, PDO::PARAM_INT);
			$j->execute();			
		}
    }
	
	// Fonction d'administration permettant de modifier un diner
	public function updateDinerAdmin($diner){
		/*$c = Base::getConnection();
		$req = $c->prepare("UPDATE diner SET idu = :newIdu, nom = :newNom, date = :newDate, lieu = :newLieu, description = :newDescr, prix = :newPrix, capacite = :newCapa WHERE idd = :idd");  
		$req->bindParam (':newIdu',$diner->idu, PDO::PARAM_INT);
		$req->bindParam (':newNom',$diner->nom, PDO::PARAM_STR);
		$req->bindParam (':newDate',$diner->date, PDO::PARAM_STR);
		$req->bindParam (':newLieu',$diner->lieu, PDO::PARAM_STR);
		$req->bindParam (':newDescr',$diner->desc, PDO::PARAM_STR);
		$req->bindParam (':newPrix', $diner->prix, PDO::PARAM_INT);
		$req->bindParam (':newCapa',$diner->capacite, PDO::PARAM_INT);
		$req->bindParam (':idd',$diner->idd, PDO::PARAM_INT); 
		$req->execute(); */
    }

	// Fonction permettant de supprimer un diner
    public function deleteFruit($idf){
        $c = Base::getConnection();
        $query = $c->prepare("DELETE from fruit where idf=:idf");
        $query->bindParam(':idf', $idf, PDO::PARAM_INT);
        $query->execute();
		
		$query2 = $c->prepare("DELETE from asso_fruit_critere where idf=:idf");
		$query2->bindParam(":idf", $idf, PDO::PARAM_INT);
		$query2->execute();
		
        return $query->rowCount();
    }
}