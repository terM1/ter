<?php
/**
* Fichier de Modele
*/

if (file_exists('base.php')){
    include_once 'base.php';
}
else {
    include_once '../base.php';
}
/**
* Classe permettant d'accéder à la table critere de la base de donnée
* La table critere permet de définir les différents critères de diner pouvant être assignés (1 seul par diner)
*/
class critere {

	/**
    * identifiant du critere
    * @access private
    *  @var integer
    */
    private $idc;
	
	/**
	* identifiant de la classification du critere
	* @acces private
	*	@var string
	*/
	private $idcc;

	/**
    * nom du critere
    * @access private
    *  @var string
    */
    private $nom;

    public function __construct() {
        
    }

	// Fonction de getter
    public function __get($attr_name) {
        if (property_exists( __CLASS__, $attr_name)) {
            return $this->$attr_name;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
        throw new Exception($emess, 45);
    }

	// Fonction de setter
    public function __set($attr_name, $attr_val) {
        if (property_exists( __CLASS__, $attr_name)) {
            $this->$attr_name = $attr_val;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    }

	// Fonction qui retourne la liste des criteres
    public function getAll(){
        $c=Base::getConnection();
        $query = $c->prepare("select * from critere");
        $dbres = $query->execute();
        $d = $query->fetchAll();
        $tab = array();
        foreach ($d as $key => $value) {
            $a = new critere();
            $a->idc = $value['idc'];
			$a->idcc = $value['idcc'];
            $a->nom = $value['nom'];
            $tab[] = $a;
        }
        return $tab;
    }
	
	// Fonction qui retourne la liste des critères ayant une catégorie donnée
	public function getAllByIdcc($idcc){
        $c = Base::getConnection();
		$query = $c->query("SELECT * FROM critere where idcc =".$idcc);
		$tab = array();
        while ($k = $query->fetch()){
            $a = new critere();
            $a->idc = $k['idc'];
            $a->idcc = $k['idcc'];
            $a->nom = $k['nom'];
            $tab[] = $a;
        }
        return $tab;
	}
	
	// Fonction permettant d'ajouter un nouveau critère dans la base
	public function insert($nom, $idcc){
		$c = Base::getConnection();
		$query = $c->prepare("insert into critere (idcc, nom)
							values (:idcc, :nom)");
		$query->bindParam (':idcc', $idcc, PDO::PARAM_INT);
		$query->bindParam (':nom',$nom, PDO::PARAM_STR);
		$query->execute();
	}

}