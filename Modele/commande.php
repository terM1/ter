<?php
/**
* Fichier de Modele
*/

include_once 'image.php';
include_once 'fruit.php';
include_once 'critere.php';


if (file_exists('base.php')){
    include_once 'base.php';
}
else {
    include_once '../base.php';
}
/**
* Classe permettant d'accéder à la table diner de la base de donnée
* La table diner définit les diners proposés sur le site.
*/
class commande{
    /**
    * identifiant du diner
    * @access private
    *  @var integer
    */
    private $idcom;

    /**
    * identifiant de l'hôte
    * @access private
    *  @var integer
    */
    private $idu;

    /**
    * nom du diner
    * @access private
    *  @var int
    */
    private $idf;
    
    /**
    * date du diner
    * @access private
    *  @var date
    */
    private $date;

    /**
     * date du diner
     * @access private
     *  @var int
     */
    private $quantite;

    /**
    * prix du diner
    * @access private
    *  @var integer
    */
    private $prixTotal;

	public function __construct() {
    	
  	}

	// Fonction de getter
  	public function __get($attr_name) {
    	if (property_exists( __CLASS__, $attr_name)) {
      		return $this->$attr_name;
    	}
    	$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    	throw new Exception($emess, 45);
  	}

	// Fonction de setter
  	public function __set($attr_name, $attr_val) {
   		if (property_exists( __CLASS__, $attr_name)) {
      		$this->$attr_name = $attr_val;
    	}
    	$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    }


    // Fonction permettant d'ajouter un nouveau diner dans la base
    public function insert($idu, $idf, $quantite, $prixTotal , $date){
        $c = Base::getConnection();
        $query = $c->prepare("insert into commande(idu,idf,quantite,date,prixTotal)
                          values(:idu,:idf,:quantite,:date,:prixTotal)");
        $query->bindParam (':idu',$idu, PDO::PARAM_INT);
        $query->bindParam (':idf',$idf, PDO::PARAM_STR);
        $query->bindParam (':prix',$prixTotal, PDO::PARAM_INT);
        $query->bindParam (':date',$date, PDO::PARAM_STR);
        $query->bindParam (':quantite',$quantite, PDO::PARAM_INT);
        $query->execute();

        $this->idcom = $c->LastInsertId('commande');
    }

    // Fonction retournant la liste des diners à venir pour un hote donné
    public function getAllCommandes($idu){
        $listeD = array();
        $c = Base::getConnection();
        $res = $c->query("select * from commande where idu = ".$idu);

        while ($donnees = $res->fetch()){
            $d = new commande();
            $d->idcom = $donnees['idcom'];
            $d->idu = $donnees['idu'];
            $d->idf = $donnees['idf'];
            $d->quantite = $donnees['quantite'];
            $d->date = $donnees['date'];
            $d->prixTotal = $donnees['prixTotal'];
            $listeD[] = $d;
        }
        return $listeD;
    }

	// Fonction retournant les informations d'un diner choisi par son idd
    public function getInfosCommande($idcom){
        $c = Base::getConnection();
        $d = new diner();
        $reponse = $c->query('SELECT * FROM commande WHERE idcom ='.$idcom);
        $donnees = $reponse->fetch();
        $d->idcom = $idcom;
        $d->idu = $donnees['idu'];
        $d->date = $donnees['date'];
        $d->idf = $donnees['idf'];
        $d->quantite = $donnees['quantite'];
        $d->prixTotal = $donnees['prixTotal'];
        return $d;
    }

	// Fonction retournant les diners répondants à la recherche
	public function rechercher($idu,$nom,$date ,$prix, $capa, $critere, $lieu){
        /*$c = base::getConnection();
        $request = "SELECT * FROM diner where 1";
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Debut Test <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        if (!empty($nom)){
            $nom = strip_tags(htmlentities($nom));
            $request.=" and nom LIKE '%".$nom."%'";
        }

        if (!empty($date)){
            $date = strip_tags(htmlentities($date));
            $request.=" and date='".$date."'";
        }
        if (!empty($prix)){
            $prix = strip_tags(htmlentities($prix));
            $request.=" and prix <= ".$prix." order by prix ASC";
        }

        if (!empty($capa)) {
            $capa = strip_tags(htmlentities($capa));
            $request.=" and capacite <= ".$capa;
        }

        if (!empty($critere)) {
            $critere = strip_tags(htmlentities($critere));
            $request.=" and idc=".$critere;
        }

        if (!empty($lieu)){
            $lieu = strip_tags(htmlentities($lieu));
            $request.=" and lieu LIKE '%".$lieu."%'";
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Fin Test <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        $query = $c->prepare($request); //where nom LIKE '%".$nom."'"
        $dbres = $query->execute();
        $d = $query->fetchAll();
        $tab = array();
        foreach ($d as $key => $donnees) {
            $d = new diner();
            $c = new critere();
            $i = new image();
            $d->idd = $donnees['idd'];
            $d->idu = $donnees['idu'];
            $d->nom = $donnees['nom'];
            $d->date = $donnees['date'];
            $d->desc = $donnees['description'];
            $d->lieu = $donnees['lieu'];
            $d->prix = $donnees['prix'];
            $d->capacite = $donnees['capacite'];
            $d->critere = $c->getId($donnees['idc']);
            $d->photos = $i->getAdd($donnees['idd']);
            $tab[] = $d;
        }
        return $tab;*/
    }
	
	// Fonction permettant de modifier un diner
    public function updateCommande($commande){
        $c = Base::getConnection();
        if(isset($commande->idcom) && isset($commande->idu) && isset($commande->idf) && isset($commande->quantite) && isset($commande->date) && isset($commande->prixTotal)){
			if($commande->quantite <= 0){
				deleteCommande($commande->idcom);
			}else{
                $req = $c->prepare("UPDATE commande SET idu = :nidu, idf = :nidf, quantite = :nquantite, date = :ndate, prixTotal = :nprixtotal WHERE idcom = :idcom");  
                $req->bindParam (':nidu',$commande->idu, PDO::PARAM_INT);
                $req->bindParam (':nidf',$$commande->idf, PDO::PARAM_INT);
                $req->bindParam (':nquantite',$commande->quantite, PDO::PARAM_INT);
				$req->bindParam (':ndate',$commande->date, PDO::PARAM_STR);
                $req->bindParam (':nprixtotal',$commande->prixTotal, PDO::PARAM_INT);
				$req->bindParam (':idcom',$commande->idcom, PDO::PARAM_INT);
                $req->execute();
                return $req->rowCount();
			}
        } else { return 0;}
        
    }
	
	// Fonction d'administration permettant de modifier un diner
	public function updateDinerAdmin($diner){
		/*$c = Base::getConnection();
		$req = $c->prepare("UPDATE diner SET idu = :newIdu, nom = :newNom, date = :newDate, lieu = :newLieu, description = :newDescr, prix = :newPrix, capacite = :newCapa WHERE idd = :idd");  
		$req->bindParam (':newIdu',$diner->idu, PDO::PARAM_INT);
		$req->bindParam (':newNom',$diner->nom, PDO::PARAM_STR);
		$req->bindParam (':newDate',$diner->date, PDO::PARAM_STR);
		$req->bindParam (':newLieu',$diner->lieu, PDO::PARAM_STR);
		$req->bindParam (':newDescr',$diner->desc, PDO::PARAM_STR);
		$req->bindParam (':newPrix', $diner->prix, PDO::PARAM_INT);
		$req->bindParam (':newCapa',$diner->capacite, PDO::PARAM_INT);
		$req->bindParam (':idd',$diner->idd, PDO::PARAM_INT); 
		$req->execute(); */
    }

	// Fonction permettant de supprimer un diner
    public function deleteCommande($idcom){
        $c = Base::getConnection();
        $query = $c->prepare("DELETE from commande where idcom=:idcom");
        $query->bindParam(':idcom', $idcom, PDO::PARAM_INT);
        $query->execute();
        return $query->rowCount();
    }
}