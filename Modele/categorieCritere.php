<?php
/**
* Fichier de Modele
*/

if (file_exists('base.php')){
    include_once 'base.php';
}
else {
    include_once '../base.php';
}
/**
* Classe permettant d'accéder à la table categoriecritere de la base de donnée
* La table categoriecritere permet de grouper des criteres
*/
class categorieCritere {
	
	/**
	* identifiant de la classification de la categorie du critere
	* @acces private
	*	@var int
	*/
	private $idcc;

	/**
    * nom de la categorie
    * @access private
    *  @var string
    */
    private $nom;

    public function __construct() {
        
    }

	// Fonction de getter
    public function __get($attr_name) {
        if (property_exists( __CLASS__, $attr_name)) {
            return $this->$attr_name;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
        throw new Exception($emess, 45);
    }

	// Fonction de setter
    public function __set($attr_name, $attr_val) {
        if (property_exists( __CLASS__, $attr_name)) {
            $this->$attr_name = $attr_val;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    }

	// Fonction qui retourne la liste des catégories criteres
    public function getAll(){
        $c=Base::getConnection();
        $query = $c->prepare("select * from categoriecritere");
        $c = base::getConnection();
        $dbres = $query->execute();
        $d = $query->fetchAll();
        $tab = array();
        foreach ($d as $key => $value) {
            $a = new categorieCritere();
 			$a->idcc = $value['idcc'];
            $a->nom = $value['nom'];
            $tab[] = $a;
        }
        return $tab;
    }
	
	// Fonction permettant d'ajouter une nouvelle catégorie de critère dans la base
	public function insert($nom){
		$c = Base::getConnection();
		$query = $c->prepare("insert into categoriecritere (nom)
							values (:nom)");
		$query->bindParam (':nom',$nom, PDO::PARAM_STR);
		$query->execute();
	}
}