<?php
/**
* Fichier de Vue
*/
if (file_exists("index.php")){
    $linkIndex = './';
}
else {
    $linkIndex = '../';
}

if (file_exists($linkIndex.'Controleur/FuncController.php')){
	include_once $linkIndex.'Controleur/FuncController.php';
}
/**
* Classe permettant la mise en forme et le dynamisme de la barre de navigation principale
* Il y a 3 barres possibles en fonction de la connexion et des droits d'accès
*/
class menuBarre{

    //Récupération des string
    private $tabAction = array( "barreVisiteur" => "barreVisiteur",
        "barreAbonne" => "barreAbonne",
        "barreAdmin" => "barreAdmin");

    public function __construc(){
        
    }

    //Affichage de la barre passé en paramètre
    public function affichage($sel){
        $nom = $this->tabAction[$sel];
        return $this->$nom();
    }

//Barre pour les visiteurs (sans compte)
public function barreVisiteur(){
	if (file_exists("index.php")){
		$linkIndex = './';
    }else {
        $linkIndex = '../';
    }
		
	$f = new FuncController();
	
	//Chargement de la liste des catégorie de critères pour être utilisé dans une select-bar
	$ccritere = $f->getAllCategorieCritere();
	$ccriteres = "";
	foreach($ccritere as $t) {
		$ccriteres .='<fieldset><legend>'.$t->nom.'</legend>';
		$critere = $f->getAllCritereByIdcc($t->idcc);
		$criteres = "";
		foreach($critere as $x) {
			$criteres .='<div><input type="radio" name="'.$x->idcc.'" id="'.$x->idc.'"><label for="'.$x->idcc.'">'.$x->nom.'</label></div>';
		} 
		$ccriteres .= $criteres.'</fieldset>';
	}
		
	$res='<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					</button>
					<a class="navbar-brand" href="'.$linkIndex.'index.php">Accueil</a>
				</div>
				
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="nav navbar-nav">
						<li class="nav-item">
							<a data-toggle="modal" data-target="#fruitModal" style="cursor:pointer">Rechercher un fruit</a>
						</li>
						<!-- Modal -->
<!-- Formulaire de recherche d un fruit -->
						<div class="modal fade" id="fruitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Rechercher un fruit</h4>
									</div>
									<form method="post" action="'.$linkIndex.'Vue/recherche.php">
										<div class="modal-body">
											<p><h4>Sélectionnez les critères que vous désirez !</h4></p>
											<div class="input-group">
												<span class="input-group-addon">Nom</span>
												<input name="nom" type="text" class="form-control" placeholder="Nom du fruit" aria-describedby="basic-addon1">
											</div>
											<div class="input-group">
												<span class="input-group-addon">Prix maximum :</span>
												<input name="prix" class="form-control" type="number" name="prix" min="0" max="1000" step="10" placeholder="Prix du fruit à l\'unité">
											</div>
											<div>'.
												$ccriteres
											.'</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
											<button class="btn btn-info" type="submit">Recherche</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<li class="nav-item">
							<a style="cursor:pointer" data-toggle="modal" data-target="#adminModal" style="cursor:pointer">Contacter un Administrateur</a>
						</li>
						<!-- Modal -->
<!-- Formulaire d envoie de mail à un administrateur -->
						<div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel">Nouveau Message</h4>
									</div>
									<div class="modal-body">
										<form method="post" action="'.$linkIndex.'Site.php?a=contactAdmin">
											<div class="form-group">
												<label for="recipient-name" class="control-label">Votre mail:</label>
												<input type="mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="recipient-name" name="mail">
											</div>
											<div class="form-group">
												<label for="recipient-name" class="control-label">Objet:</label>
												<input type="text" class="form-control" id="recipient-name" name="objet">
											</div>
											<div class="form-group">
												<label for="message-text" class="control-label">Message:</label>
												<textarea class="form-control" id="message-text" name="msg" style="resize: vertical;" readonly>Cette fonctionnalité envoye correctement le mail, mais il ne sera pas recu. Il faut ajouter des paramètre comme des clefs sur le DNS.</textarea>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
												<button class="btn btn-info" type="submit">Envoyer</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</ul>
					 <ul class="nav navbar-nav navbar-right">
						<li>
							<a class="nav-link" data-toggle="modal" data-target="#compteModal" style="cursor:pointer">Créer un Compte</a>
						<!-- Modal -->
<!-- Formulaire de création d un compte par un visiteur -->
						<div class="modal fade" id="compteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Création d\'un compte client</h4>
									</div>
									<form method="post" action="'.$linkIndex.'Site.php?a=creerCompteClient">
										<div class="modal-body">
											Veuillez renseigner vos informations
											<div class="form-group">
												<label for="message-text" class="control-label">Nom:</label>
												<input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="nom">
											</div>
										<div class="form-group">
											<label for="message-text" class="control-label">Prénom:</label>
											<input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="prenom" >
										</div>
										<div class="form-group">
											<label for="message-text" class="control-label">Adresse:</label>
											<textarea class="form-control" id="recipient-name" name="addresse" style="resize: vertical;"></textarea>
										</div>
										<div class="form-group">
											<label for="message-text" class="control-label">Code Postal:</label>
											<input type="text" pattern="[0-9]{5}" class="form-control" id="recipient-name" name="codePostal">
										</div>
										<div class="form-group">
											<label for="message-text" class="control-label">Ville:</label>
											<input type="text" class="form-control" id="recipient-name" name="ville">
										</div>
										<div class="form-group">
											<label for="message-text" class="control-label">N° de téléphone:</label>
											<input type="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" id="recipient-name" name="tel">
										</div>
										<div class="form-group">
											<label for="recipient-name" class="control-label">Email:</label>
											<input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="recipient-name" name="mail">
										</div>
										<div class="form-group">
											<label for="message-text" class="control-label">Mot de passe:</label>
											<input type="password" class="form-control" id="recipient-name" name="mdp">
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
											<button id="bouton" class="btn btn-info" type="submit">Créer</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						</li>
						 <li>
							<a class="nav-link" data-toggle="modal" data-target="#connexionModal" style="cursor:pointer"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Connexion</a>
						</li>
						<!-- Modal -->
<!-- Formulaire de connexion -->
						<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="connexionModal">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Connexion</h4>
									</div>
									<div class="modal-body">
										Veuillez entrer vos informations de compte
										<form method="post" action="'.$linkIndex.'Modele/Connexion.php" id="form_connexion">
											<div class="form-group">
												<label for="recipient-name" class="control-label">Email:</label>
												<input type="email" class="form-control" name="email" id="recipient-name">
											</div>
											<div class="form-group">
												<label for="message-text" class="control-label">Mot de passe:</label>
												<input type="password" class="form-control" id="recipient-name" name="mdp">
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
												<button type="submit" class="btn btn-info" >Connexion</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</div>
				</nav>
				<script>
				$("#myModal").on("shown.bs.modal",function () {
					$("#myInput").focus()
				})
				</script>';
	return $res;
}

public function  barreAbonne() {
    if (file_exists("index.php")){
        $linkIndex = './';
    }else{
        $linkIndex = '../';
    }
	
	$f = new FuncController();
    
	//Chargement des informations de l'utilisateur connecté
	$utilisateur = $f->getInfoClientByIdu($_SESSION['idu']);
	$idu = $_SESSION['idu'];
    foreach ($utilisateur as $uti) {
        $nomAbo = $uti['nom'];
        $prenomAbo = $uti['prenom'];
        $mailAbo = $uti['email'];
        $telAbo = $uti['telephone'];
        $addAbo = $uti['addresse'];
        $cpAbo = $uti['codePost'];
        $villeAbo = $uti['ville'];
		$soldeAbo = $uti['solde'];
    }
	
	//Chargement de la liste des catégorie de critères pour être utilisé dans une select-bar
	$ccritere = $f->getAllCategorieCritere();
	$ccriteres = "";
	foreach($ccritere as $t) {
		$ccriteres .='<fieldset><legend>'.$t->nom.'</legend>';
		$critere = $f->getAllCritereByIdcc($t->idcc);
		$criteres = "";
		foreach($critere as $x) {
			$criteres .='<div><input type="radio" name="'.$x->idcc.'" id="'.$x->idc.'"><label for="'.$x->idcc.'">'.$x->nom.'</label></div>';
		} 
		$ccriteres .= $criteres.'</fieldset>';
	}

     $res='<nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        </button>
                        <a class="navbar-brand" href="'.$linkIndex.'index.php">Accueil</a>
                    </div>
                    
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                                <a data-toggle="modal" data-target="#fruitModal" style="cursor:pointer">Rechercher un fruit</a>
                            </li>
                            <!-- Modal -->
<!-- Formulaire de recherche d un fruit -->
						<div class="modal fade" id="fruitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Rechercher un fruit</h4>
									</div>
									<form method="post" action="'.$linkIndex.'Vue/recherche.php">
										<div class="modal-body">
											<p><h4>Sélectionnez les critères que vous désirez !</h4></p>
											<div class="input-group">
												<span class="input-group-addon">Nom</span>
												<input name="nom" type="text" class="form-control" placeholder="Nom du fruit" aria-describedby="basic-addon1">
											</div>
											<div class="input-group">
												<span class="input-group-addon">Prix maximum :</span>
												<input name="prix" class="form-control" type="number" name="prix" min="0" max="1000" step="10" placeholder="Prix du fruit à l\'unité">
											</div>
											<div>'.
												$ccriteres
											.'</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
											<button class="btn btn-info" type="submit">Recherche</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<li class="nav-item">
							<a style="cursor:pointer" data-toggle="modal" data-target="#adminModal" style="cursor:pointer">Contacter un Administrateur</a>
						</li>
                            <!-- Modal -->
<!-- Formulaire d envoie de mail à un admin -->
                            <div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="exampleModalLabel">Nouveau Message</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="'.$linkIndex.'Site.php?a=contactAdmin">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Votre mail:</label>
                                                    <input type="mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="recipient-name" name="mail">
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Objet:</label>
                                                    <input type="text" class="form-control" id="recipient-name" name="objet">
                                                </div>
											<div class="form-group">
												<label for="message-text" class="control-label">Message:</label>
												<textarea class="form-control" id="message-text" name="msg" style="resize: vertical;" readonly>Cette fonctionnalité envoye correctement le mail, mais il ne sera pas recu. Il faut ajouter des paramètre comme des clefs sur le DNS.</textarea>
											</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button class="btn btn-info" type="submit">Envoyer</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul>
                         <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mon compte <span class="caret"></span></a>
<!-- Menu déroulant d interface de gestion de compte -->
                                <ul class="dropdown-menu">
                                    <li><a class="nav-link" data-toggle="modal" data-target="#compteModal" style="cursor:pointer">Mes infos</a></li>
                                    <li><a href="'.$linkIndex.'Vue/mesCommandes.php">Mes Commandes</a></li>
                                </ul>
                                
                            <!-- Modal -->
<!-- Formulaire de modification des informations du compte connecté -->
                            <div class="modal fade" id="compteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Informations et modification du compte pour : '.$mailAbo.'</h4>
                                        </div>
                                        <form method="post" action="'.$linkIndex.'Site.php?a=modifCompteAbonne">
                                            <div class="modal-body">
                                                Modifiez vos informations ici
												<input type="hidden" name="idu" value="'.$idu.'">
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Nom:</label>
                                                    <input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="nom" value="'.$nomAbo.'">
                                                </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Prénom:</label>
                                                <input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="prenom" value="'.$prenomAbo.'" >
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Adresse:</label>
                                                <textarea class="form-control" id="recipient-name" name="addresse" style="resize: vertical;">'.$addAbo.'</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Code Postal:</label>
                                                <input type="text" pattern="[0-9]{5}" class="form-control" id="recipient-name" name="codePostal" value="'.$cpAbo.'">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Ville:</label>
                                                <input type="text" class="form-control" id="recipient-name" name="ville" value="'.$villeAbo.'">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">N° de téléphone:</label>
                                                <input type="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" id="recipient-name" name="tel" value="'.$telAbo.'">
                                            </div>
											<div class="form-group">
                                                <label for="message-text" class="control-label">Solde:</label>
                                                <input type="number" class="form-control" id="recipient-name" name="solde" value="'.$soldeAbo.'" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Nouveau mot de passe:</label>
                                                <input type="password" class="form-control" id="recipient-name" name="mdp1">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">répéter le Nouveau mot de passe:</label>
                                                <input type="password" class="form-control" id="recipient-name" name="mdp2">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Mot de passe actuel (pour valider les changements) :</label>
                                                <input type="password" class="form-control" id="recipient-name" name="mdpV">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                <button id="bouton" class="btn btn-info" type="submit">Modifier</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="modal" data-target="#DeconnexionModal" style="cursor:pointer"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Déconnexion</a>
                            </li>
                            <!-- Modal -->
<!-- Formulaire de déconnexion -->
                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="DeconnexionModal">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Deconnexion</h4>
                                        </div>
                                        <div class="modal-body">
                                            Voulez vous vraiment vous deconnecter ?
                                            <form method="post" action="'.$linkIndex.'Modele/Deconnexion.php" id="form_deconnexion">
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-info" >Deconnexion</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                    </nav>
                    <script>
                    $("#myModal").on("shown.bs.modal", function () {
                        $("#myInput").focus()
                    })
                    </script>';
     return $res;
}
 
public function  barreAdmin() {
	if (file_exists("index.php")){
		$linkIndex = './';
	}else{
		$linkIndex = '../';
	}
	 
	$f = new FuncController();
	
	//Chargement de la liste des utilisateur pour être utilisé dans une select-bar
	$users = $f->getAllUsers();
	$options = "";
	foreach ($users as $utilisateur){
		$options .= '<option value="'.$utilisateur['idu'].'">'.$utilisateur['idu'].' - '.$utilisateur['prenom'].' '.$utilisateur['nom'].' - '.$utilisateur['email'].'</option>';
	}
	
	//Chargement de la liste des catégorie de critères pour être utilisé dans une select-bar
	$ccritere = $f->getAllCategorieCritere();
	$ccriteres = "";
	foreach($ccritere as $t) {
		$ccriteres .='<fieldset><legend>'.$t->nom.'</legend>';
		$critere = $f->getAllCritereByIdcc($t->idcc);
		$criteres = "";
		foreach($critere as $x) {
			$criteres .='<div><input type="radio" name="'.$x->idcc.'" id="'.$x->idc.'" value="'.$x->idc.'"><label for="'.$x->idcc.'">'.$x->nom.'</label></div>';
		} 
		$ccriteres .= $criteres.'</fieldset>';
	}
	
	//Chargement de la liste des catégorie de critères pour être utilisé dans une select-bar
	$listeccritere = $f->getAllCategorieCritere();
	$listeccriteres = "";
	foreach($listeccritere as $t) {
		$listeccriteres .='<option value="'.$t->idcc.'">'.$t->nom.'</option>';
	}
	
	//Chargement de la liste des acces pour être utilisé dans une select-bar
	$accesList = $f->getAllAcces();
	$acces = "";
	foreach ($accesList as $acc){
		$acces .= '<option value="'.$acc['ida'].'">'.$acc['nom'].'</option>';
	}
	    
	//Chargement des informations du compte connecté
    $utilisateur = $f->getInfoClientByIdu($_SESSION['idu']);
	$idu = $_SESSION['idu'];
    foreach ($utilisateur as $uti) {
        $nomAbo = $uti['nom'];
        $prenomAbo = $uti['prenom'];
        $mailAbo = $uti['email'];
        $telAbo = $uti['telephone'];
        $addAbo = $uti['addresse'];
        $cpAbo = $uti['codePost'];
        $villeAbo = $uti['ville'];
		$soldeAbo = $uti['solde'];
    }
	
	$res='<nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        </button>
                        <a class="navbar-brand" href="'.$linkIndex.'index.php">Accueil</a>
                    </div>
                    
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                                <a data-toggle="modal" data-target="#fruitModal" style="cursor:pointer">Rechercher un fruit</a>
                            </li>
                             <!-- Modal -->
<!-- Formulaire de recherche d un fruit -->
                            <div class="modal fade" id="fruitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Rechercher un fruit</h4>
										</div>
										<form method="post" action="'.$linkIndex.'Vue/recherche.php">
										<div class="modal-body">
											<p><h4>Sélectionnez les critères que vous désirez !</h4></p>
											<div class="input-group">
												<span class="input-group-addon">Nom</span>
												<input name="nom" type="text" class="form-control" placeholder="Nom du fruit" aria-describedby="basic-addon1">
											</div>
											<div class="input-group">
												<span class="input-group-addon">Prix maximum :</span>
												<input name="prix" class="form-control" type="number" name="prix" min="0" max="1000" step="10" placeholder="Prix du fruit à l\'unité">
											</div>
											<div>'.
												$ccriteres
											.'</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
											<button class="btn btn-info" type="submit">Recherche</button>
										</div>
									</form>
                                    </div>
                                </div>
                            </div>
							<!-- Modal -->

                            <li class="nav-item">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
<!-- Menu Déroulant de l interface d administration -->
								<ul class="dropdown-menu">
									<li><a data-toggle="modal" data-target="#creerCompteAdm" style="cursor:pointer">Créer un compte</a></li>
									<li><a href="'.$linkIndex.'Vue/modifCompteAdm.php">Modifier un compte utilisateur</a></li>
									<li><a data-toggle="modal" data-target="#modifSolde" style="cursor:pointer">Modifier un solde</a></li>
									<li><a data-toggle="modal" data-target="#supprimerCompteAdm" style="cursor:pointer">Supprimer un compte</a></li>
									<li><a data-toggle="modal" data-target="#creerFruitAdm" style="cursor:pointer">Créer un fruit</a></li>
									<li><a href="'.$linkIndex.'Vue/modifFruitAdm.php">Modifier un fruit</a></li>
									<li><a data-toggle="modal" data-target="#creerCategorieCritere" style="cursor:pointer">Créer une catégorie critère</a></li>
									<li><a data-toggle="modal" data-target="#creerCritere" style="cursor:pointer">Créer un critère</a></li>
								</ul>
                            </li>
                        </ul>
                         <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mon compte <span class="caret"></span></a>
<!-- Menu déroulant de l interface de gestion de compte -->
                                <ul class="dropdown-menu">
                                    <li><a class="nav-link" data-toggle="modal" data-target="#compteModal" style="cursor:pointer">Mes infos</a></li>
                                    <li><a href="'.$linkIndex.'Vue/mesCommandes.php">Mes commandes</a></li>

                                </ul>
                                
                            <!-- Modal -->
<!-- Formulaire de modification du compte connecté -->
                            <div class="modal fade" id="compteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Informations et modification du compte pour : '.$mailAbo.'</h4>
                                        </div>
                                        <form method="post" action="'.$linkIndex.'Site.php?a=modifCompteAbonne">
                                            <div class="modal-body">
                                                Modifiez vos informations ici
												<input type="hidden" name="idu" value="'.$idu.'">
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Nom:</label>
                                                    <input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="nom" value="'.$nomAbo.'">
                                                </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Prénom:</label>
                                                <input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="prenom" value="'.$prenomAbo.'" >
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Adresse:</label>
                                                <textarea class="form-control" id="recipient-name" name="addresse" style="resize: vertical;">'.$addAbo.'</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Code Postal:</label>
                                                <input type="text" pattern="[0-9]{5}" class="form-control" id="recipient-name" name="codePostal" value="'.$cpAbo.'">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Ville:</label>
                                                <input type="text" class="form-control" id="recipient-name" name="ville" value="'.$villeAbo.'">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">N° de téléphone:</label>
                                                <input type="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" id="recipient-name" name="tel" value="'.$telAbo.'">
                                            </div>
											<div class="form-group">
                                                <label for="message-text" class="control-label">Solde:</label>
                                                <input type="number" class="form-control" id="recipient-name" name="solde" value="'.$soldeAbo.'" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Nouveau mot de passe:</label>
                                                <input type="password" class="form-control" id="recipient-name" name="mdp1">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">répéter le Nouveau mot de passe:</label>
                                                <input type="password" class="form-control" id="recipient-name" name="mdp2">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Mot de passe actuel (pour valider les changements) :</label>
                                                <input type="password" class="form-control" id="recipient-name" name="mdpV">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                <button id="bouton" class="btn btn-info" type="submit">Modifier</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="modal" data-target="#DeconnexionModal" style="cursor:pointer"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Déconnexion</a>
                            </li>
                            <!-- Modal -->
<!-- Formulaire de déconnexion -->
                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="DeconnexionModal">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Deconnexion</h4>
                                        </div>
                                        <div class="modal-body">
                                            Voulez vous vraiment vous deconnecter ?
                                            <form method="post" action="'.$linkIndex.'Modele/Deconnexion.php" id="form_deconnexion">
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-info" >Deconnexion</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
					
					<!-- Modal -->
<!-- Formulaire de creation de compte via Admin -->
							<div class="modal fade" id="creerCompteAdm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Création d\'un compte client</h4>
										</div>		
											<form method="post" action="'.$linkIndex.'Site.php?a=creerCompteClientAdmin">
												<div class="modal-body">
													Veuillez renseigner vos informations
													<div class="form-group">
														<label for="message-text" class="control-label">Nom:</label>
														<input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="nom">
													</div>
													<div class="form-group">
														<label for="message-text" class="control-label">Prénom:</label>
														<input type="text" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" class="form-control" id="recipient-name" name="prenom" >
													</div>
													<div class="form-group">
														<label for="message-text" class="control-label">Adresse:</label>
														<textarea class="form-control" id="recipient-name" name="addresse" style="resize: vertical;"></textarea>
													</div>
													<div class="form-group">
														<label for="message-text" class="control-label">Code Postal:</label>
														<input type="text" pattern="[0-9]{5}" class="form-control" id="recipient-name" name="codePostal">
													</div>
													<div class="form-group">
														<label for="message-text" class="control-label">Ville:</label>
														<input type="text" class="form-control" id="recipient-name" name="ville">
													</div>
													<div class="form-group">
														<label for="message-text" class="control-label">N° de téléphone:</label>
														<input type="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" id="recipient-name" name="tel">
													</div>
													<div class="form-group">
														<label for="recipient-name" class="control-label">Email:</label>
														<input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="recipient-name" name="mail">
													</div>
													<div class="form-group">
														<label for="message-text" class="control-label">Mot de passe:</label>
														<input type="password" class="form-control" id="recipient-name" name="mdp">
													</div>
													<div class="form-group">
														<label for="message-text" class="control-label">Droits:</label>
														<select class="form-control" id="recipient-name" name="droit">'.
															$acces
														.'</select>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
													<button id="bouton" class="btn btn-info" type="submit">Créer</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Modal -->
<!-- Formulaire de creation de fruit via admin -->
                            <div class="modal fade" id="creerFruitAdm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="exampleModalLabel">Créer un nouveau fruit</h4>
										</div>
										<div class="modal-body">
                                            <form method="post" action="'.$linkIndex.'Site.php?a=creerFruitAdmin" enctype="multipart/form-data">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Nom</span>
                                                    <input name="nom" type="text" class="form-control" placeholder="Nom du fruit" aria-describedby="basic-addon1" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+">
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">Description</span>
                                                    <textarea name="desc" type="text-area" class="form-control" placeholder="Description du fruit" aria-describedby="basic-addon1" style="resize: vertical;"></textarea>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">Prix:</span>
                                                    <input name="prix" class="form-control" type="number" name="prix" min="0" max="1000" placeholder="Prix du fruit">
                                                </div>
                                                <div>'.
													$ccriteres
												.'</div>
                                                <div class="input-group">
                                                    <br/><label for="image">Image : </label>
													<input name="image" type="file" id="image"/>
												</div>
                                                <div class="alert alert-warning" role="alert"><small class="alert_info">
                                                    L\'image insérée doit avoir des dimensions inférieures à 5000x5000px et une taille inférieure à 500Mo .</small >
                                                </div >
												<div class="modal-footer" >
                                                    <button type = "button" class="btn btn-default" data-dismiss="modal"> Fermer</button >
                                                    <button class="btn btn-info" type = "submit" > Envoyer</button >
                                                </div >
                                            </form >
                                        </div>
									</div>
								</div>
                            </div>
							
							<!-- Modal -->

<!-- Formulaire de création de critère -->
							<div class="modal fade" id="creerCritere" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Création d\'un critère</h4>
										</div>		
											<form method="post" action="'.$linkIndex.'Site.php?a=creerCritere">
												<div class="modal-body">
													Veuillez renseigner les informations
													<div class="form-group">
														<span class="input-group-addon">Catégorie</span>
														<select class="form-control" id="recipient-name" name="idcc">'.
															$listeccriteres
														.'</select>
													</div>
													<div class="form-group">
														<span class="input-group-addon">Nom</span>
														<input name="nom" type="text" class="form-control" placeholder="Nom du critère" aria-describedby="basic-addon1" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+">
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
													<button id="bouton" class="btn btn-info" type="submit">Créer</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Modal -->
	
<!-- Formulaire de création de catégorie de critère -->
							<div class="modal fade" id="creerCategorieCritere" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Création d\'une catégorie de critère</h4>
										</div>		
											<form method="post" action="'.$linkIndex.'Site.php?a=creerCategorieCritere">
												<div class="modal-body">
													Veuillez renseigner les informations
													<div class="form-group">
														<span class="input-group-addon">Nom</span>
														<input name="nom" type="text" class="form-control" placeholder="Nom de la catégorie critère" aria-describedby="basic-addon1" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+">
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
													<button id="bouton" class="btn btn-info" type="submit">Créer</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Modal -->
		
<!-- Formulaire de suppression de compte -->
							<div class="modal fade" id="supprimerCompteAdm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Supprimer un compte</h4>
										</div>		
											<form method="post" action="'.$linkIndex.'Site.php?a=supprimerUtilisateurAdm">
												<div class="modal-body">
													<div class="input-group">
														<span class="input-group-addon">Compte:</span>
														<select class="form-control" name="orga">'.
															$options
														.'</select>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
													<button id="bouton" class="btn btn-info" type="submit">Supprimer</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Modal -->
<!-- Formulaire de modification de solde -->
							<div class="modal fade" id="modifSolde" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Modifier un solde</h4>
										</div>		
											<form method="post" action="'.$linkIndex.'Site.php?a=modifSolde">
												<div class="modal-body">
													<div class="input-group">
														<span class="input-group-addon">Compte:</span>
														<select class="form-control" name="idu">'.
															$options
														.'</select>
													</div>
													<div class=modal-body">
														<div class="input-group">
															<span class="input-group-addon">Modification:</span>
															<input class="form-control" type="number" name="solde" step="1" placeholder="Utiliser - pour retirer">
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
													<button id="bouton" class="btn btn-info" type="submit">Modifier</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							
                    </nav>
                    <script>
                    $("#myModal").on("shown.bs.modal", function () {
                        $("#myInput").focus()
                    })
                    </script>';
     return $res;
 }

}