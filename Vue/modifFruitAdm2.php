<?php 
/**
* Fichier de Vue
* Permet de modifier un diner d'un utilisateur
*/
if (file_exists("index.php")){
    $linkIndex = './';
}
else {
    $linkIndex = '../';
}
include_once $linkIndex.'Controleur/FuncController.php';

// Chargement de la barre de navigation
session_start();
$barre = "barreVisiteur";
if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
{
	$grade=$_SESSION['acces'];
	$id=$_SESSION['idu'];
	if ($grade == 'Administrateur'){
	}else{
		header('Location:./../index.php');
	}
	switch($grade) {
	case "Abonne":
		$barre = "barreAbonne";
		break;
	case "Administrateur":
		$barre = "barreAdmin";
		break;
	}
}else{
	if(isset($grade)){
		unset($grade);
		unset($id);
	}
	header('Location:./../index.php');
}

if($_POST['fruit'] == 0){
	header('Location: modifFruitAdm.php');
}else{
	// Chargement du formulaire de modification d'un diner
	$idf = $_POST['fruit'];
	$f = new FuncController();
	$fruit = $f->getInfoFruitByIdf($idf);
	
	// Chargement de la liste des criteres selon categorie
	$ccritere = $f->getAllCategorieCritere();
	$ccriteres = "";
	foreach($ccritere as $t) {
		$ccriteres .='<fieldset><legend>'.$t->nom.'</legend>';
		$critere = $f->getAllCritereByIdcc($t->idcc);
		$criteres = "";
		foreach($critere as $x) {
			$criteres .='<div><input type="radio" name="'.$x->idcc.'" id="'.$x->idc.'" value="'.$x->idc.'"';
			if(in_array($x->idc, $fruit->criteres)){
				$criteres .= ' checked="checked"';
			}
			$criteres .= '><label for="'.$x->idcc.'">'.$x->nom.'</label></div>';
		} 
		$ccriteres .= $criteres.'</fieldset>';
	}
	$formInfo = '<h4>Zone d\'édition</h4><input class="form-control" type="hidden" name="idf" id="idf" value="'.$idf.'"><div class="input-group"><span class="input-group-addon">Nom</span><input name="nom" type="text" class="form-control" placeholder="Nom du fruit" aria-describedby="basic-addon1" value="'. $fruit->nom .'"></div><div class="input-group"><span class="input-group-addon">Description</span><textarea name="desc" type="text-area" class="form-control" placeholder="Description du fruit" aria-describedby="basic-addon1" style="resize: vertical;">' . $fruit->desc .'</textarea></div><div class="input-group"><span class="input-group-addon">Prix:</span><input name="prix" class="form-control" type="number" name="prix" min="0" max="1000" placeholder="Prix du fruit" value="'. $fruit->prix .'"></div><div>'.$ccriteres.'</div><div class="input-group"><br/><label for="image">Nouvelle  image (ne pas toucher pour conserver l\'ancienne) : </label><input name="image" type="file" id="image"/></div><div class="alert alert-warning" role="alert"><small class="alert_info">L\'image insérée doit avoir des dimensions inférieures à 5000x5000px et une taille inférieure à 500Mo .</small ></div ><div class="modal-footer" ><button type = "button" class="btn btn-default" data-dismiss="modal"> Fermer</button ><button class="btn btn-info" type = "submit" > Envoyer</button ></div >';
	// Redirection avec le formulaire
	header('Location: modifFruitAdm.php?a='.$formInfo);
}
?>

