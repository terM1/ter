<?php
/**
* Fichier de Vue
* Permet d'afficher les commandes d'un compte
*/

include_once 'menuBarre.php';
include_once '../Controleur/FuncController.php';

// Chargement de la barre de navigation
session_start();
$barre = "barreVisiteur";
$id = 0;
if(isset($_SESSION['acces']) && isset($_SESSION['idu']))
{
    $grade=$_SESSION['acces'];
    $id=$_SESSION['idu'];

    switch($grade) {
        case "Abonne":
            $barre = "barreAbonne";
            break;
        case "Administrateur":
            $barre = "barreAdmin";
            break;
    }
}else{
    if(isset($grade))
        unset($grade);
	
	header('Location:./../index.php');
}

if (file_exists("index.php")){
            $linkIndex = './';
        }
else {
    $linkIndex = '../';
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Dîner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSS -->
    <link type="text/css" href="../Css/menuBarre.css" rel="stylesheet" />
    <link type="text/css" href="../Css/index.css" rel="stylesheet" />
    <link type="text/css" href="../bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../bootstrap/datepicker/css/datepicker.css" rel="stylesheet"/>
    <link type="text/css" href="../slider/css/slider.css" rel="stylesheet"/>



    <!--JS-->
    <script language="javascript" type="text/javascript" src="../bootstrap/dist/js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="../bootstrap/dist/js/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="../bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
    <script language="javascript" type="text/javascript" src="../Js/index.js"></script>
    <script language="javascript" type="text/javascript" src="../Js/menuBarre.js"></script>
    <script language="javascript" type="text/javascript" src="../slider/js/bootstrap-slider.js"></script>
    <script language="javascript" type="text/javascript" src="../Js/rating.js"></script>

</head>

<body id="body">
<?php
$v = new menuBarre();
echo $v->affichage($barre);

// Affichage d'un potentiel message
$message = '';
if (isset($_GET['message'])){
    $message = $_GET['message'];
    echo '<div class="alert alert-success" role="alert">'.$message.'</div>';
}
?>
<div class="container">
    <div class="bloc-2">
        <div class="page-header">
            <h2>Mes Commandes</h2>
        </div>
        <table class="table table-striped">
            <thead>
              <tr>
                <th>Numero de commande</th>
                <th>Date</th>
                <th>Produit</th>
                <th>Quantité</th>
                <th>Prix total</th>
				<th></th>
              </tr>
            </thead>
            <tbody>
                
                
         <?php
			$f = new FuncController();
			
			// Chargement du formulaire de la liste des diners prévus par le compte
            $listeDC = $f->getAllCommandesByIdu($id);
            foreach ($listeDC as $key => $commande) {  
                echo '<tr>
                        <td>'.$commande->idcom.'</td>
						<td>'.$commande->date.'</td>
                        <td>'.getInfosFruit($commande->idf).'</td>
                        <td>'.$commande->quantite.'</td>
                        <td>'.$commande->prixTotal.' €</td>
                        <td><a class="btn btn-primary" href="#" role="button" data-toggle="modal" data-target="#modifier'.$commande->idcom.'" style="cursor:pointer">Modifier</a></td>
                        <td><a class="btn btn-danger" href="#" role="button" data-toggle="modal" data-target="#supprimer'.$commande->idcom.'" style="cursor:pointer">Supprimer</a></td>
                    </tr>
                    
                     <div class="modal fade" id="modifier'.$fruit->idf.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <form method="post" action="'.$linkIndex.'Site.php?a=modifierCommande&idcom='.$commande->idcom.'">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="exampleModalLabel">Commande n°'.$commande->idcom.'</h4>
                                    </div>
                                    <div class="modal-body">
										<p>Date : '.$commande->date.'</p>
										<p>Produit : '.getInfosFruit($commande->idf).'</p>
                                        <div class="input-group">
                                            <span class="input-group-addon">Quantité:</span>
                                            <input name="quantite" class="form-control" type="number" name="quantite" min="0" max="200" step="1" placeholder="Quantité" value='.$commande->quantite.'>
                                        </div>
                                        <p>Prix : '.$commande->prixTotal.'</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="submit">Modifier la commande</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="modal fade" id="supprimer'.$commande->idcom.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <form method="post" action="'.$linkIndex.'Site.php?a=annulerCommande&idcom='.$commande->idcom.'">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="exampleModalLabel">Commande n°'.$commande->idcom.'</h4>
                                    </div>
                                    <div class="modal-body">
										<p>Date : '.$commande->date.'</p>
										<p>Produit : '.getInfosFruit($commande->idf).'</p>
                                        <div class="input-group">
                                            <span class="input-group-addon">Quantité:</span>
                                            <input name="quantite" class="form-control" type="number" name="quantite" min="0" max="200" step="1" placeholder="Quantité" value='.$commande->quantite.' disable>
                                        </div>
                                        <p>Prix : '.$commande->prixTotal.'</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-danger" type="submit">Annuler la commande</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>';
            }
        ?>
                
            </tbody>
        </table>
    </div>
</div>
</body>
