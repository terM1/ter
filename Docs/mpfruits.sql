-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Dim 07 Janvier 2018 à 22:42
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mpfruits`
--

-- --------------------------------------------------------

--
-- Structure de la table `acces`
--

CREATE TABLE `acces` (
  `ida` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `acces`
--

INSERT INTO `acces` (`ida`, `nom`) VALUES
(1, 'Abonne'),
(2, 'Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `asso_fruit_critere`
--

CREATE TABLE `asso_fruit_critere` (
  `ida` int(11) NOT NULL,
  `idf` int(11) NOT NULL,
  `idc` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `asso_fruit_critere`
--

INSERT INTO `asso_fruit_critere` (`ida`, `idf`, `idc`) VALUES
(6, 13, 1),
(5, 13, 5),
(4, 13, 16),
(7, 13, 8),
(8, 15, 15),
(9, 15, 9),
(10, 16, 5),
(11, 17, 5),
(12, 18, 1);

-- --------------------------------------------------------

--
-- Structure de la table `categoriecritere`
--

CREATE TABLE `categoriecritere` (
  `idcc` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categoriecritere`
--

INSERT INTO `categoriecritere` (`idcc`, `nom`) VALUES
(1, 'Saison'),
(2, 'Géographie'),
(3, 'Type'),
(4, 'Graine'),
(5, 'Aspect'),
(6, 'Cuisinable');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `idcom` int(11) NOT NULL,
  `idu` int(11) NOT NULL,
  `idf` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `date` date NOT NULL,
  `prixTotal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `critere`
--

CREATE TABLE `critere` (
  `idc` int(11) NOT NULL,
  `idcc` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `critere`
--

INSERT INTO `critere` (`idc`, `idcc`, `nom`) VALUES
(1, 3, 'Agrume'),
(2, 2, 'Europe'),
(3, 2, 'Asie'),
(4, 2, 'Afrique'),
(5, 2, 'Oceanie'),
(6, 2, 'Amerique du Sud'),
(7, 2, 'Amerique du Nord'),
(8, 4, 'Pepin'),
(9, 4, 'Noyau'),
(10, 5, 'Grappe'),
(11, 3, 'Baie'),
(16, 1, 'Hiver'),
(15, 1, 'Ete');

-- --------------------------------------------------------

--
-- Structure de la table `fruit`
--

CREATE TABLE `fruit` (
  `idf` int(11) NOT NULL,
  `nom` varchar(120) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `prix` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `fruit`
--

INSERT INTO `fruit` (`idf`, `nom`, `description`, `prix`) VALUES
(2, 'Zero animal', 'Je vous invite &agrave; venir d&eacute;couvrir ou red&eacute;couvrir la cuisine vegan. Le repas sera compos&eacute; de A &agrave; Z de mati&egrave;res non animales et enti&egrave;rement BIO. ', 20),
(3, 'Tacos veggie', 'Repas sur le th&egrave;me du tacos v&eacute;g&eacute;tariens. Apprenez &agrave; appr&eacute;cier les l&eacute;gumes sans qu\'ils soient accompagn&eacute;s de viande ! ', 30),
(4, 'Dessert a volonte', 'Avis &agrave; tous les becs sucr&eacute;s ! J\'organise un d&icirc;ner compos&eacute; uniquement de desserts. Je vous propose donc gateaux, tartes, mousses et fruits sous toutes leurs formes. ', 20),
(5, 'Ramen maison', 'Je vous propose une soir&eacute;e sur le th&egrave;me des ramen, nouilles japonaises en bouillon. Tout sera fait maison et pr&eacute;parer minute. En dessert, je vous propose un trio de mochi beurre de cacahu&egrave;te, tsubuan (p&acirc;te de haricots azuki sucr&eacute;e) et matcha.', 30),
(16, 'testadmin', 'le grand retour', 7),
(17, 'testadmin', 'le grand retour', 7),
(15, 'Patate', 'crue', 10),
(14, 'Patate', 'crue', 10),
(13, 'Kiwi', 'petit oeuf poilu', 5),
(18, 'qkfjdbqozfgqo', 'nsofs eifb qefhg', 4);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `idi` int(11) NOT NULL,
  `idf` int(11) NOT NULL,
  `adresse` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`idi`, `idf`, `adresse`) VALUES
(1, 1, 'Images/vegan-dinner-surf-food.'),
(2, 2, 'Images/vegan-dinner-surf-food.jpg'),
(3, 3, 'Images/landscape-1470239458-vegeterian-dinners.jpg'),
(4, 4, 'Images/wedding-dessert-bar-urban-unveiled.jpg'),
(5, 5, 'Images/Ramen-porc-salé-705x469.jpg'),
(7, 18, '');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idu` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `addresse` varchar(50) NOT NULL,
  `codePost` varchar(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `solde` float NOT NULL,
  `ida` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idu`, `email`, `mdp`, `nom`, `prenom`, `addresse`, `codePost`, `ville`, `telephone`, `solde`, `ida`) VALUES
(1, 'admin@admin.fr', '1c6d66a83a2a06ba491464cb9dced2c2832f3675', 'Admin', 'Admin', 'Admin', '0', 'Admin', '0000000000', 20, 2),
(2, 'martin.dupont@gmail.com', '1b11500549ed65fd91a62052e1f890cab599e049', 'Martin', 'Dupont', '82 rue de la Roquette ', '75011', 'Paris', '0698485632', 50, 1),
(4, 'victorbreton1@gmail.com', '85bc6acdc149ab641ba14357756f117c590e25cb', 'Moi', 'Moi', '13 rue jolliot curie', '91130', 'Gif-sur-Yvette', '0123456789', 300, 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acces`
--
ALTER TABLE `acces`
  ADD PRIMARY KEY (`ida`);

--
-- Index pour la table `asso_fruit_critere`
--
ALTER TABLE `asso_fruit_critere`
  ADD PRIMARY KEY (`ida`);

--
-- Index pour la table `categoriecritere`
--
ALTER TABLE `categoriecritere`
  ADD PRIMARY KEY (`idcc`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`idcom`);

--
-- Index pour la table `critere`
--
ALTER TABLE `critere`
  ADD PRIMARY KEY (`idc`);

--
-- Index pour la table `fruit`
--
ALTER TABLE `fruit`
  ADD PRIMARY KEY (`idf`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`idi`),
  ADD KEY `idd` (`idf`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idu`),
  ADD KEY `ida` (`ida`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `acces`
--
ALTER TABLE `acces`
  MODIFY `ida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `asso_fruit_critere`
--
ALTER TABLE `asso_fruit_critere`
  MODIFY `ida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `categoriecritere`
--
ALTER TABLE `categoriecritere`
  MODIFY `idcc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `idcom` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `critere`
--
ALTER TABLE `critere`
  MODIFY `idc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `fruit`
--
ALTER TABLE `fruit`
  MODIFY `idf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `idi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
